package vito.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import vito.utils.Constants;

import java.util.Arrays;

/**
 * @author Vitaliy Ippolitov
 */
@Controller
abstract class BaseController {

    final ResponseEntity validateBindingResult(BindingResult bindingResult) {
        try {
            if (Arrays.asList(bindingResult.getAllErrors().get(0).getCodes())
                .contains("typeMismatch.java.math.BigDecimal")) {
                return ResponseEntity.badRequest().body(Constants.PARSE_DECIMAL_ERR_RU);
            }
        } catch (NullPointerException ex) {
            return ResponseEntity.badRequest().body(Constants.BINDING_SERVER_ERR_RU);
        }
        return ResponseEntity.badRequest().body(Constants.BINDING_ERR_RU);
    }
}
