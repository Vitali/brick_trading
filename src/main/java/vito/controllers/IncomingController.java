package vito.controllers;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import vito.models.entity.Incoming;
import vito.models.entity.Product;
import vito.models.vo.IncomingVO;
import vito.services.IncomingService;
import vito.services.ProductService;
import vito.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static vito.utils.Constants.CUSTOM_DATE_FORMAT;

/**
 * @author Vitaliy Ippolitov
 */
@Controller
@RequestMapping(value = "/incoming")
public class IncomingController extends BaseController {

    @Autowired
    private IncomingService incomingService;

    @Autowired
    private ProductService productService;

    @RequestMapping(method = RequestMethod.GET)
    public String renderPage(Model model) {
        List<IncomingVO> incomingVOs =
            Optional.ofNullable(incomingService.getData())
                .orElse(new ArrayList<>(0))
                .stream()
                .map(t -> IncomingVO.builder()
                    .id(t.getId())
                    .productId(Optional.ofNullable(t.getProduct()).map(Product::getId).orElse(null))
                    .productCaption(Optional.ofNullable(t.getProduct()).map(Product::getCaption).orElse(null))
                    .volume(t.getVolume())
                    .shipper(t.getShipper())
                    .dateIncoming(new DateTime(t.getDateIncoming()).toString(DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT)))
                    .carNumber(t.getCarNumber())
                    .build()
                )
                .collect(toList());

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        boolean hasAdminRights = auth.getAuthorities().stream().map(
            t -> Constants.ADMIN_ROLE_CAPTION.equals(t.getAuthority())
        ).findFirst().orElse(false);
        model.addAttribute("isAdminMode", hasAdminRights);
        model.addAttribute("incomingVOs", incomingVOs);
        model.addAttribute("productVOs", productService.getData());
        return "incoming";
    }

    @RequestMapping(value = "/addOrUpdate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addOrUpdate(IncomingVO incomingVO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return super.validateBindingResult(bindingResult);
        }
        Product product = null;
        if (incomingVO.getProductId() != null) {
            product = productService.findOne(incomingVO.getProductId());
        }
        final DateTime dateTimeIncoming = Optional.ofNullable(incomingVO.getDateIncoming())
            .filter(t -> !"".equals(t))
            .map(t -> DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT).parseDateTime(t))
            .orElse(new DateTime());

        final Incoming incoming = new Incoming(product, incomingVO.getVolume(), incomingVO.getShipper(),
            dateTimeIncoming.toDate(), incomingVO.getCarNumber());
        incoming.setId(incomingVO.getId());
        String body = incomingService.addOrUpdate(incoming);
        if (body.isEmpty()) {
            return ResponseEntity.ok("");
        }
        return ResponseEntity.badRequest().body(body);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity delete(IncomingVO incomingVO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return super.validateBindingResult(bindingResult);
        }
        String body = incomingService.delete(incomingVO.getId());
        if (body.isEmpty()) {
            return ResponseEntity.ok("");
        }
        return ResponseEntity.badRequest().body(Constants.REMOVE_ERR_RU + body);
    }

    @Deprecated
    @RequestMapping(value = "/find")
    @ResponseBody
    public String find(String caption) {
        return incomingService.find(caption);
    }
}