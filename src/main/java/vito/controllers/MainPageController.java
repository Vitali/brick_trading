package vito.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vito.utils.Constants;

/**
 * @author Vitaliy Ippolitov
 */
@Controller
@RequestMapping(value = "/")
public class MainPageController extends BaseController {

    @RequestMapping(method = RequestMethod.GET)
    public String renderPage(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        boolean hasAdminRights = auth.getAuthorities().stream().map(
            t -> Constants.ADMIN_ROLE_CAPTION.equals(t.getAuthority())
        ).findFirst().orElse(false);
        model.addAttribute("isAdminMode", hasAdminRights);
        return "mainPage";
    }
}
