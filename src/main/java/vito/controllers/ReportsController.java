package vito.controllers;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vito.models.entity.Product;
import vito.models.vo.ReportVO;
import vito.services.CustomerService;
import vito.services.ProductService;
import vito.services.ReportService;

import java.util.List;

import static vito.utils.Constants.CUSTOM_DATE_FORMAT;

/**
 * @author Vitaliy Ippolitov
 */
@Controller
@RequestMapping(value = ReportsController.REPORTS)
class ReportsController extends BaseController {

    static final String REPORTS = "/reports";

    @Autowired
    private ReportService reportService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ProductService productService;

    private enum ReportType {
        TOTAL_CONSUMPTION,
        DEBTS,
        TOTAL_INCOMING,
        BALANCE,
        OTHER_CONSUMPTION,
        OTHER_INCOMING
    }

    /**
     * Add parameters into model
     * <p>
     * It fills the default fields for all report types
     *
     * @param model      - model that will be filled
     * @param requestUrl - url address that will be used as action
     * @param reportType - ReportType enum element
     * @return template name
     */
    private String reportGreetingPage(Model model, String requestUrl, ReportType reportType, ReportVO reportVO) {
        model.addAttribute("requestUrl", REPORTS + requestUrl);
        model.addAttribute("reportType", reportType);
        DateTime now = DateTime.now();
        reportVO.setDateFrom(new DateTime(now.minusMonths(1)).toString(DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT)));
        reportVO.setDateTo(new DateTime(now).toString(DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT)));
        model.addAttribute("reportVO", reportVO);
        return "report";
    }

    @RequestMapping(value = "/totalConsumption", method = RequestMethod.GET)
    public String totalConsumption(Model model) {
        model.addAttribute("customerVOs", customerService.getData());
        return reportGreetingPage(model, "/totalConsumption/request", ReportType.TOTAL_CONSUMPTION, new ReportVO());
    }

    @RequestMapping(value = "/totalConsumption/request", method = RequestMethod.GET)
    public String totalConsumption(Model model, ReportVO reportVO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getAllErrors().toString());
        } else {
            final String errorMsg = reportService.totalConsumption(reportVO);
            if (!errorMsg.isEmpty()) {
                model.addAttribute("error", errorMsg);
            }
        }
        model.addAttribute("reportVO", reportVO);
        return "reportDynamicContent";
    }

    @RequestMapping(value = "/debts", method = RequestMethod.GET)
    public String debts(Model model) {
        model.addAttribute("customerVOs", customerService.getData());
        return reportGreetingPage(model, "/debts/request", ReportType.DEBTS, new ReportVO());
    }

    @RequestMapping(value = "/debts/request", method = RequestMethod.GET)
    public String debts(Model model, ReportVO reportVO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getAllErrors().toString());
        } else {
            final String errorMsg = reportService.debts(reportVO);
            if (!errorMsg.isEmpty()) {
                model.addAttribute("error", errorMsg);
            }
        }
        model.addAttribute("reportVO", reportVO);
        return "reportDynamicContent";
    }

    @RequestMapping(value = "/totalIncoming", method = RequestMethod.GET)
    public String totalIncoming(Model model) {
        List<Product> products = productService.getData();
        model.addAttribute("productVOs", products);
        return reportGreetingPage(model, "/totalIncoming/request", ReportType.TOTAL_INCOMING, new ReportVO());
    }

    @RequestMapping(value = "/totalIncoming/request", method = RequestMethod.GET)
    public String totalIncoming(Model model, ReportVO reportVO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getAllErrors().toString());
        } else {
            final String errorMsg = reportService.totalIncoming(reportVO);
            if (!errorMsg.isEmpty()) {
                model.addAttribute("error", errorMsg);
            }
        }
        model.addAttribute("reportVO", reportVO);
        return "reportDynamicContent";
    }

    @RequestMapping(value = "/balance", method = RequestMethod.GET)
    public String balance(Model model) {
        List<Product> products = productService.getData();
        model.addAttribute("productVOs", products);
        return reportGreetingPage(model, "/balance/request", ReportType.BALANCE, new ReportVO());
    }

    @RequestMapping(value = "/balance/request", method = RequestMethod.GET)
    public String balance(Model model, ReportVO reportVO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getAllErrors().toString());
        } else {
            final String errorMsg = reportService.balance(reportVO);
            if (errorMsg == null || !errorMsg.isEmpty()) {
                model.addAttribute("error", errorMsg);
            }
        }
        model.addAttribute("reportVO", reportVO);
        return "reportDynamicContent";
    }

    @RequestMapping(value = "/otherConsumption", method = RequestMethod.GET)
    public String otherConsumption(Model model) {
        return reportGreetingPage(model, "/otherConsumption/request", ReportType.OTHER_CONSUMPTION, new ReportVO());
    }

    @RequestMapping(value = "/otherConsumption/request", method = RequestMethod.GET)
    public String otherConsumption(Model model, ReportVO reportVO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getAllErrors().toString());
        } else {
            final String errorMsg = reportService.otherConsumption(reportVO);
            if (!errorMsg.isEmpty()) {
                model.addAttribute("error", errorMsg);
            }
        }
        model.addAttribute("reportVO", reportVO);
        return "reportDynamicContent";
    }

    @RequestMapping(value = "/otherIncoming", method = RequestMethod.GET)
    public String otherIncoming(Model model) {
        return reportGreetingPage(model, "/otherIncoming/request", ReportType.OTHER_INCOMING, new ReportVO());
    }

    @RequestMapping(value = "/otherIncoming/request", method = RequestMethod.GET)
    public String otherIncoming(Model model, ReportVO reportVO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getAllErrors().toString());
        } else {
            final String errorMsg = reportService.otherIncoming(reportVO);
            if (!errorMsg.isEmpty()) {
                model.addAttribute("error", errorMsg);
            }
        }
        model.addAttribute("reportVO", reportVO);
        return "reportDynamicContent";
    }
}