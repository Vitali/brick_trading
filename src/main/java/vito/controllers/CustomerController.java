package vito.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import vito.models.entity.Customer;
import vito.services.CustomerService;
import vito.utils.Constants;

import java.util.List;

/**
 * @author Vitaliy Ippolitov
 */
@Controller
@RequestMapping(value = "/customers")
public class CustomerController extends BaseController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(method = RequestMethod.GET)
    public String renderPage(Model model) {
        List<Customer> customers = customerService.getData();
        model.addAttribute("customerVOs", customers);
        return "customers";
    }

    @RequestMapping(value = "/addOrUpdate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addOrUpdate(Customer customer, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return super.validateBindingResult(bindingResult);
        }
        final String body = customerService.addOrUpdate(customer);
        if (body.isEmpty()) {
            return ResponseEntity.ok("");
        }
        return ResponseEntity.badRequest().body(body);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity delete(Customer customer, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return super.validateBindingResult(bindingResult);
        }
        final String body = customerService.delete(customer.getId());
        if (body.isEmpty()) {
            return ResponseEntity.ok("");
        }
        return ResponseEntity.badRequest().body(Constants.REMOVE_ERR_RU + body);
    }

    @Deprecated
    @RequestMapping(value = "/find", method = RequestMethod.GET)
    @ResponseBody
    public String find(String caption) {
        return customerService.find(caption);
    }
}