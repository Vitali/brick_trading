package vito.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import vito.models.entity.Product;
import vito.services.ProductService;
import vito.utils.Constants;

import java.util.List;

/**
 * @author Vitaliy Ippolitov
 */
@Controller
@RequestMapping(value = "/products")
public class ProductController extends BaseController {

    @Autowired
    private ProductService productService;

    @RequestMapping(method = RequestMethod.GET)
    public String renderPage(Model model) {
        List<Product> products = productService.getData();
        model.addAttribute("productVOs", products);
        return "products";
    }

    @RequestMapping(value = "/addOrUpdate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addOrUpdate(Product product, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return super.validateBindingResult(bindingResult);
        }
        String body = productService.addOrUpdate(product);
        if (body.isEmpty()) {
            return ResponseEntity.ok("");
        }
        return ResponseEntity.badRequest().body(body);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity delete(Product product, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return super.validateBindingResult(bindingResult);
        }
        String body = productService.delete(product.getId());
        if (body.isEmpty()) {
            return ResponseEntity.ok("");
        }
        return ResponseEntity.badRequest().body(Constants.REMOVE_ERR_RU + body);
    }

    @Deprecated
    @RequestMapping(value = "/find")
    @ResponseBody
    public String find(String caption) {
        return productService.find(caption);
    }
}