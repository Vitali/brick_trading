package vito.controllers;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import vito.models.entity.IncomingOther;
import vito.models.vo.IncomingOtherVO;
import vito.services.IncomingOtherService;
import vito.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static vito.utils.Constants.CUSTOM_DATE_FORMAT;

/**
 * @author Vitaliy Ippolitov
 */
@Controller
@RequestMapping(value = "/incomingOther")
public class IncomingOtherController extends BaseController {

    @Autowired
    private IncomingOtherService incomingOtherService;

    @RequestMapping(method = RequestMethod.GET)
    public String renderPage(Model model) {
        List<IncomingOtherVO> incomingOtherVOs =
            Optional.ofNullable(incomingOtherService.getData())
                .orElse(new ArrayList<>(0))
                .stream()
                .map(t -> IncomingOtherVO.builder()
                    .id(t.getId())
                    .caption(t.getCaption())
                    .amount(t.getAmount())
                    .dateIncomingOther(new DateTime(t.getDateIncomingOther()).toString(DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT)))
                    .build()
                )
                .collect(toList());

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        boolean hasAdminRights = auth.getAuthorities().stream().map(
            t -> Constants.ADMIN_ROLE_CAPTION.equals(t.getAuthority())
        ).findFirst().orElse(false);
        model.addAttribute("isAdminMode", hasAdminRights);
        model.addAttribute("incomingOtherVOs", incomingOtherVOs);
        return "incomingOther";
    }

    @RequestMapping(value = "/addOrUpdate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addOrUpdate(IncomingOtherVO incomingOtherVO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return super.validateBindingResult(bindingResult);
        }
        IncomingOther incomingOther = new IncomingOther();
        incomingOther.setId(incomingOtherVO.getId());
        incomingOther.setCaption(incomingOtherVO.getCaption());
        DateTime dateTimeIncomingOth = Optional.ofNullable(incomingOtherVO.getDateIncomingOther())
            .map(t -> !"".equals(t) ? DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT).parseDateTime(t) : new DateTime())
            .orElse(new DateTime());
        incomingOther.setDateIncomingOther(dateTimeIncomingOth.toDate());
        incomingOther.setAmount(incomingOtherVO.getAmount());
        String body = incomingOtherService.addOrUpdate(incomingOther);
        if (body.isEmpty()) {
            return ResponseEntity.ok("");
        }
        return ResponseEntity.badRequest().body(body);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity delete(IncomingOtherVO incomingOtherVO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return super.validateBindingResult(bindingResult);
        }
        String body = incomingOtherService.delete(incomingOtherVO.getId());
        if (body.isEmpty()) {
            return ResponseEntity.ok("");
        }
        return ResponseEntity.badRequest().body(Constants.REMOVE_ERR_RU + body);
    }

    @Deprecated
    @RequestMapping(value = "/find")
    @ResponseBody
    public String find(String caption) {
        return incomingOtherService.find(caption);
    }
}