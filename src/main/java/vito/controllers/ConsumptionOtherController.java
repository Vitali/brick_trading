package vito.controllers;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import vito.models.entity.ConsumptionOther;
import vito.models.vo.ConsumptionOtherVO;
import vito.services.ConsumptionOtherService;
import vito.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static vito.utils.Constants.CUSTOM_DATE_FORMAT;

/**
 * @author Vitaliy Ippolitov
 */
@Controller
@RequestMapping(value = "/consumptionOther")
public class ConsumptionOtherController extends BaseController {

    @Autowired
    private ConsumptionOtherService consumptionOtherService;

    @RequestMapping(method = RequestMethod.GET)
    public String renderPage(Model model) {
        List<ConsumptionOtherVO> consumptionOtherVOs =
            Optional.ofNullable(consumptionOtherService.getData())
                .orElse(new ArrayList<>(0))
                .stream()
                .map(t -> ConsumptionOtherVO.builder()
                    .id(t.getId())
                    .caption(t.getCaption())
                    .amount(t.getAmount())
                    .dateConsumptionOther(new DateTime(t.getDateConsumptionOther())
                        .toString(DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT)))
                    .build()
                )
                .collect(toList());

        model.addAttribute("consumptionOtherVOs", consumptionOtherVOs);
        return "consumptionOther";
    }

    @RequestMapping(value = "/addOrUpdate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addOrUpdate(ConsumptionOtherVO consumptionOtherVO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return super.validateBindingResult(bindingResult);
        }
        ConsumptionOther consumptionOther = new ConsumptionOther();
        consumptionOther.setId(consumptionOtherVO.getId());
        consumptionOther.setCaption(consumptionOtherVO.getCaption());
        DateTime dateTimeIncomingOth = Optional.ofNullable(consumptionOtherVO.getDateConsumptionOther())
            .map(t -> !"".equals(t) ? DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT).parseDateTime(t) : new DateTime())
            .orElse(new DateTime());
        consumptionOther.setDateConsumptionOther(dateTimeIncomingOth.toDate());
        consumptionOther.setAmount(consumptionOtherVO.getAmount());
        String body = consumptionOtherService.addOrUpdate(consumptionOther);
        if (body.isEmpty()) {
            return ResponseEntity.ok("");
        }
        return ResponseEntity.badRequest().body(body);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity delete(ConsumptionOtherVO consumptionOtherVO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return super.validateBindingResult(bindingResult);
        }
        final String body = consumptionOtherService.delete(consumptionOtherVO.getId());
        if (body.isEmpty()) {
            return ResponseEntity.ok("");
        }
        return ResponseEntity.badRequest().body(Constants.REMOVE_ERR_RU + body);
    }

    @Deprecated
    @RequestMapping(value = "/find")
    @ResponseBody
    public String find(String caption) {
        return consumptionOtherService.find(caption);
    }
}