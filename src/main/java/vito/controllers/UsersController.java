package vito.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import vito.models.auth.Role;
import vito.models.dao.UserDao;
import vito.models.entity.User;
import vito.services.RoleService;
import vito.utils.Constants;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Vitaliy Ippolitov
 */
@RestController
@RequestMapping("/users")
@PreAuthorize("hasRole('ADMIN')")
public class UsersController extends BaseController {

    @Autowired
    UserDao userDao;

    @Autowired
    RoleService roleService;

    @RequestMapping(method = RequestMethod.GET)
    public List<User> getUsers() {
        final List<User> result = new ArrayList<>();
        userDao.findAll().forEach(result::add);

        return result;
    }

    @RequestMapping(method = RequestMethod.POST)
    public User addUser(final String username, final String password, final String passwordConfirm) {
        //no empty fields allowed
        if (username.isEmpty() || password.isEmpty() || passwordConfirm.isEmpty()) {
            return null;
        }

        //passwords should match
        if (!password.equals(passwordConfirm)) {
            return null;
        }

        final Set<Role> roles = new HashSet<>();
        roles.add(roleService.findOne(Constants.USER_ROLE_ID));

        return userDao.save(new User(username, password, roles));
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView getUserForm() {
        return new ModelAndView("addUser");
    }
}