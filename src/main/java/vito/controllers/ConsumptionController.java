package vito.controllers;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import vito.models.entity.Consumption;
import vito.models.entity.Customer;
import vito.models.entity.Goods;
import vito.models.vo.ConsumptionVO;
import vito.models.vo.GoodsVO;
import vito.services.ConsumptionService;
import vito.services.CustomerService;
import vito.services.GoodsService;
import vito.services.ProductService;
import vito.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static vito.utils.Constants.CUSTOM_DATE_FORMAT;

/**
 * @author Vitaliy Ippolitov
 */
@Controller
@RequestMapping(value = "/consumption")
public class ConsumptionController extends BaseController {

    @Autowired
    private ConsumptionService consumptionService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ProductService productService;

    @Autowired
    private GoodsService goodsService;

    @RequestMapping(method = RequestMethod.GET)
    public String renderPage(Model model) {
        final List<ConsumptionVO> consumptionVOs = consumptionService.getVoData();

        model.addAttribute("consumptionVOs", consumptionVOs);
        model.addAttribute("customerVOs", customerService.getData());
        model.addAttribute("productVOs", productService.getData());
        model.addAttribute("goodsVOs", goodsService.getData());

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        boolean hasAdminRights = auth.getAuthorities().stream().map(
            t -> Constants.ADMIN_ROLE_CAPTION.equals(t.getAuthority())
        ).findFirst().orElse(false);
        model.addAttribute("isAdminMode", hasAdminRights);
        return "consumption";
    }

    @RequestMapping(value = "/addOrUpdate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addOrUpdate(
        @RequestBody ConsumptionVO consumptionVO,
        BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            return super.validateBindingResult(bindingResult);
        }
        final DateTime dateConsumption = Optional.ofNullable(consumptionVO.getDateConsumption())
            .map(t -> !"".equals(t) ? DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT)
                .parseDateTime(t) : new DateTime())
            .orElse(new DateTime());

        final Set<Goods> goods = Optional.ofNullable(consumptionVO.getGoods())
            .orElse(new ArrayList<>(0))
            .stream().map(GoodsVO::toGoods)
            .collect(Collectors.toSet());

        final Consumption consumption = Consumption.builder()
            .isDebt(consumptionVO.getIsDebt())
            .customer(consumptionVO.getCustomerId() == null ? null
                : new Customer(consumptionVO.getCustomerId()))
            .dateConsumption(dateConsumption.toDate())
            .carNumber(consumptionVO.getCarNumber())
            .deliveryPrice(consumptionVO.getDeliveryPrice())
            .isCash(consumptionVO.getIsCash())
            .isCashless(consumptionVO.getIsCashless())
            .goods(goods)
            .build();

        consumption.setId(consumptionVO.getId());

        final String body = consumptionService.addOrUpdate(consumption);
        if (body.isEmpty()) {
            return ResponseEntity.ok("");
        }
        return ResponseEntity.badRequest().body(body);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity delete(
        @RequestBody ConsumptionVO consumptionVO,
        BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            return super.validateBindingResult(bindingResult);
        }
        String body = consumptionService.delete(consumptionVO.getId());
        if (body.isEmpty()) {
            return ResponseEntity.ok("");
        }
        return ResponseEntity.badRequest().body(Constants.REMOVE_ERR_RU + body);
    }

    @Deprecated
    @RequestMapping(value = "/find")
    @ResponseBody
    public String find(String caption) {
        return consumptionService.find(caption);
    }
}