package vito.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vito.models.dao.ConsumptionOtherDao;
import vito.models.entity.ConsumptionOther;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vitaliy Ippolitov
 */
@Service
public class ConsumptionOtherService implements IService<ConsumptionOther> {

    @Autowired
    private ConsumptionOtherDao consumptionOtherDao;

    @Override
    public List<ConsumptionOther> getData() {
        List<ConsumptionOther> result = new ArrayList<>();
        consumptionOtherDao.findAll().forEach(result::add);
        return result;
    }

    public String addOrUpdate(ConsumptionOther consumptionOther) {
        try {
            consumptionOtherDao.save(consumptionOther);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @Override
    public String delete(long id) {
        try {
            consumptionOtherDao.delete(id);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @Deprecated
    public String find(String caption) {
        try {
            final ConsumptionOther consumptionOther = consumptionOtherDao.findByCaption(caption);
            return "The consumptionOtherId id is: " + consumptionOther.getId();
        } catch (Exception ex) {
            return "ConsumptionOther not found " + ex.getMessage();
        }
    }
}
