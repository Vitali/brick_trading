package vito.services;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vito.models.entity.*;
import vito.models.vo.ReportItemVO;
import vito.models.vo.ReportRowVO;
import vito.models.vo.ReportVO;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static vito.utils.Constants.CUSTOM_DATE_FORMAT;

/**
 * @author Vitaliy Ippolitov
 */
@Service
public class ReportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportService.class);

    @Autowired
    private ConsumptionService consumptionService;

    @Autowired
    private ConsumptionOtherService consumptionOtherService;

    @Autowired
    private IncomingService incomingService;

    @Autowired
    private IncomingOtherService incomingOtherService;

    private static final String[] TOTAL_CONSUMPTION_HEADERS_CAPTION = {
        "Продавец", "Долг", "Дата", "Номер машины", "Стоимость доставки", "Наличные",
        "Безналичные", "Груз"};
    private static final String[] GOODS_HEADERS_CAPTION =
        {"Наименование товара", "Цена за единицу товара", "Количество товара"};
    private static final String[] TOTAL_INCOMING_HEADERS_CAPTION =
        {"Наименование товара", "Количество", "Отправитель", "Дата прихода", "Номер машины"};
    private static final String[] OTHER_HEADERS_CAPTION = {"Наименование", "Дата", "Сумма"};
    private static final String[] BALANCE_HEADERS_CAPTION = {"Наименование", "Количество"};
    private static final String COMMA_PLUS_REGEX = "\\s*,\\s*";

    public String totalConsumption(ReportVO reportVO) {
        final DateTime dateFrom = getDateTime(reportVO.getDateFrom());
        final DateTime dateTo = getDateTime(reportVO.getDateTo());

        reportVO.setHeaders(Arrays.asList(TOTAL_CONSUMPTION_HEADERS_CAPTION));
        final List<String> idsString = Arrays.asList(reportVO.getId().split(COMMA_PLUS_REGEX));
        final List<Long> ids = idsString.contains("") ? new ArrayList<>() : idsString.stream()
            .map(Long::valueOf).collect(toList());
        try {
            final List<Consumption> consumptions = consumptionService.getData().stream()
                .filter(t -> {
                    final Date date = t.getDateConsumption();
                    return (dateFrom == null || dateFrom.minusDays(1).toDate().before(date))
                        && (dateTo == null || dateTo.plusDays(1).toDate().after(date));
                })
                .filter(t -> (ids.isEmpty()) || ids.contains(
                    Optional.ofNullable(t.getCustomer()).map(BaseModel::getId).orElse(-1L)))
                .collect(toList());

            final List<ReportRowVO> resultRows = convertConsumption(consumptions);
            final List<ConsumptionOther> consumptionOtherRows = consumptionOtherService.getData()
                .stream().filter(t -> {
                    final Date date = t.getDateConsumptionOther();
                    return (dateFrom == null || dateFrom.minusDays(1).toDate().before(date))
                        && (dateTo == null || dateTo.plusDays(1).toDate().after(date));
                })
                .collect(toList());

            final ReportItemVO emptyItemVO = new ReportItemVO("");
            resultRows.addAll(consumptionOtherRows.stream()
                .map(t -> {
                    final List<ReportItemVO> result = new LinkedList<>();
                    result.add(new ReportItemVO(t.getCaption()));
                    result.add(emptyItemVO);
                    result.add(new ReportItemVO(Optional.ofNullable(t.getDateConsumptionOther())
                        .map(dt -> new DateTime(dt)
                            .toString(DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT)))
                        .orElse(null)));
                    result.add(emptyItemVO);
                    result.add(new ReportItemVO(
                        Optional.ofNullable(t.getAmount()).map(BigDecimal::toString).orElse("")));
                    result.add(emptyItemVO);
                    result.add(emptyItemVO);
                    result.add(emptyItemVO);
                    return new ReportRowVO(result);
                })
                .collect(toList()));
            if (resultRows.isEmpty()) {
                final List<ReportItemVO> emptyItems = new LinkedList<>();
                for (int jj = 0; jj++ < reportVO.getHeaders().size(); ) {
                    emptyItems.add(emptyItemVO);
                }
                resultRows.add(new ReportRowVO(emptyItems));
            }
            reportVO.setRows(resultRows);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            return ex.getMessage();
        }
        return "";
    }

    private List<ReportRowVO> convertConsumption(List<Consumption> consumptions) {
        return consumptions.stream()
            .map(cons -> cons.toReportRowVO(Arrays.asList(GOODS_HEADERS_CAPTION)))
            .collect(toList());
    }

    public String debts(ReportVO reportVO) {
        final DateTime dateFrom = getDateTime(reportVO.getDateFrom());
        final DateTime dateTo = getDateTime(reportVO.getDateTo());
        reportVO.setHeaders(Arrays.asList(TOTAL_CONSUMPTION_HEADERS_CAPTION));

        try {
            final List<Consumption> consumptions = consumptionService.getData().stream()
                .filter(t -> {
                    final Date date = t.getDateConsumption();
                    return (dateFrom == null || dateFrom.minusDays(1).toDate().before(date))
                        && (dateTo == null || dateTo.plusDays(1).toDate().after(date));
                })

                .filter(t -> (reportVO.getId() == null || "".equals(reportVO.getId()))
                    || Long.valueOf(reportVO.getId()).equals(
                    Optional.ofNullable(t.getCustomer()).map(BaseModel::getId).orElse(-1L)))
                .filter((Consumption::isDebt)).collect(toList());

            final List<ReportRowVO> resultRows = convertConsumption(consumptions);
            if (resultRows.isEmpty()) {
                List<ReportItemVO> emptyItems = new LinkedList<>();
                for (int jj = 0; jj++ < reportVO.getHeaders().size(); ) {
                    emptyItems.add(new ReportItemVO(""));
                }
                resultRows.add(new ReportRowVO(emptyItems));
            }
            reportVO.setRows(resultRows);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            return ex.getMessage();
        }
        return "";
    }

    public String totalIncoming(ReportVO reportVO) {
        final DateTime dateFrom = getDateTime(reportVO.getDateFrom());
        final DateTime dateTo = getDateTime(reportVO.getDateTo());

        reportVO.setHeaders(Arrays.asList(TOTAL_INCOMING_HEADERS_CAPTION));
        final List<String> idsString = Arrays.asList(reportVO.getId().split(COMMA_PLUS_REGEX));
        final List<Long> ids = idsString.contains("") ? new ArrayList<>() : idsString.stream()
            .map(Long::valueOf)
            .collect(toList());

        try {
            final List<Incoming> incomings = incomingService.getData().stream()
                .filter(t -> {
                    final Date date = t.getDateIncoming();
                    return (dateFrom == null || dateFrom.minusDays(1).toDate().before(date))
                        && (dateTo == null || dateTo.plusDays(1).toDate().after(date));
                })
                .filter(t -> (ids.isEmpty()) || ids.contains(
                    Optional.ofNullable(t.getProduct()).map(Product::getId).orElse(-1L)))
                .collect(toList());

            final List<ReportRowVO> resultRows = incomings.stream()
                .map(t -> {
                    final List<ReportItemVO> result = new LinkedList<>();
                    result.add(new ReportItemVO(
                        Optional.ofNullable(t.getProduct()).map(Product::getCaption).orElse("")));
                    result.add(new ReportItemVO(String.valueOf(t.getVolume())));
                    result.add(new ReportItemVO(String.valueOf(t.getShipper())));
                    result.add(new ReportItemVO(Optional.ofNullable(t.getDateIncoming())
                        .map(dt -> new DateTime(dt)
                            .toString(DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT)))
                        .orElse(null)));
                    result.add(new ReportItemVO(t.getCarNumber()));
                    return new ReportRowVO(result);
                })
                .collect(toList());

            if (resultRows.isEmpty()) {
                final List<ReportItemVO> emptyItems = new LinkedList<>();
                for (int jj = 0; jj++ < reportVO.getHeaders().size(); ) {
                    emptyItems.add(new ReportItemVO(""));
                }
                resultRows.add(new ReportRowVO(emptyItems));
            }
            reportVO.setRows(resultRows);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            return ex.getMessage();
        }
        return "";
    }

    public String balance(ReportVO reportVO) {
        reportVO.setHeaders(Arrays.asList(BALANCE_HEADERS_CAPTION));
        final List<String> idsString = Arrays.asList(reportVO.getId().split(COMMA_PLUS_REGEX));
        final List<Long> ids = idsString.contains("") ? new ArrayList<>() : idsString.stream()
            .map(Long::valueOf)
            .collect(toList());
        try {
            final Map<Product, Long> productsVolumeBalance = incomingService.getData().stream()
                .filter(t -> (reportVO.getId() == null || "".equals(reportVO.getId()))
                    || ids
                    .contains(Optional.ofNullable(t.getProduct()).map(Product::getId).orElse(-1L)))
                .collect(Collectors.toMap(t -> Optional.ofNullable(t.getProduct()).orElse(null),
                    (t -> Optional.ofNullable(t.getVolume()).orElse(0L)),
                    (productOne, productTwo) -> productOne + productTwo));

            final List<Goods> consumptionGoods = consumptionService.getData()
                .stream().map(Consumption::getGoods)
                .flatMap(Collection::stream)
                .collect(toList());

            final Map<Product, Long> productsVolumeConsumptions = consumptionGoods.stream()
                .filter(t -> (ids.isEmpty()) || ids.contains(
                    Optional.ofNullable(t.getProduct()).map(Product::getId).orElse(-1L)))
                .filter(t -> t.getProduct() != null && t.getVolume() != null)
                .collect(Collectors.toMap(Goods::getProduct, Goods::getVolume,
                    (productOneVolume, productTwoVolume) -> productOneVolume + productTwoVolume));

            final List<ReportRowVO> resultRows = productsVolumeBalance.entrySet().stream()
                .map(t -> {
                    final List<ReportItemVO> result = new LinkedList<>();
                    result.add(new ReportItemVO(
                        Optional.ofNullable(t.getKey()).map(Product::getCaption).orElse("")));
                    result.add(new ReportItemVO(String.valueOf(
                        t.getValue() - Optional.ofNullable(
                            productsVolumeConsumptions.get(t.getKey())
                        ).orElse(0L))
                    ));
                    return new ReportRowVO(result);
                })
                .collect(toList());

            if (resultRows.isEmpty()) {
                List<ReportItemVO> emptyItems = new LinkedList<>();
                for (int jj = 0; jj++ < reportVO.getHeaders().size(); ) {
                    emptyItems.add(new ReportItemVO(""));
                }
                resultRows.add(new ReportRowVO(emptyItems));
            }
            reportVO.setRows(resultRows);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            return ex.getMessage();
        }
        return "";
    }

    public String otherConsumption(ReportVO reportVO) {
        final DateTime dateFrom = getDateTime(reportVO.getDateFrom());
        final DateTime dateTo = getDateTime(reportVO.getDateTo());
        reportVO.setHeaders(Arrays.asList(OTHER_HEADERS_CAPTION));

        try {
            final List<ConsumptionOther> consumptionOthers = consumptionOtherService.getData()
                .stream()
                .filter(t -> {
                    Date date = t.getDateConsumptionOther();
                    return (dateFrom == null || dateFrom.minusDays(1).toDate().before(date))
                        && (dateTo == null || dateTo.plusDays(1).toDate().after(date));
                })
                .collect(toList());

            final List<ReportRowVO> resultRows = consumptionOthers.stream()
                .map(t -> {
                    final List<ReportItemVO> result = new LinkedList<>();
                    result.add(new ReportItemVO(String.valueOf(t.getCaption())));
                    result.add(new ReportItemVO(Optional.ofNullable(t.getDateConsumptionOther())
                        .map(dt -> new DateTime(dt)
                            .toString(DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT)))
                        .orElse(null)));
                    result.add(new ReportItemVO(t.getAmount().toString()));
                    return new ReportRowVO(result);
                })
                .collect(toList());

            if (resultRows.isEmpty()) {
                final List<ReportItemVO> emptyItems = new LinkedList<>();
                for (int jj = 0; jj++ < reportVO.getHeaders().size(); ) {
                    emptyItems.add(new ReportItemVO(""));
                }
                resultRows.add(new ReportRowVO(emptyItems));
            }
            reportVO.setRows(resultRows);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            return ex.getMessage();
        }
        return "";
    }

    public String otherIncoming(ReportVO reportVO) {
        final DateTime dateFrom = getDateTime(reportVO.getDateFrom());
        final DateTime dateTo = getDateTime(reportVO.getDateTo());
        reportVO.setHeaders(Arrays.asList(OTHER_HEADERS_CAPTION));

        try {
            final List<IncomingOther> incomingOthers = incomingOtherService.getData().stream()
                .filter(t -> {
                    final Date date = t.getDateIncomingOther();
                    return (dateFrom == null || dateFrom.minusDays(1).toDate().before(date))
                        && (dateTo == null || dateTo.plusDays(1).toDate().after(date));
                }).collect(toList());

            final List<ReportRowVO> resultRows = incomingOthers.stream()
                .map(t -> {
                    final List<ReportItemVO> result = new LinkedList<>();
                    result.add(new ReportItemVO(String.valueOf(t.getCaption())));
                    result.add(new ReportItemVO(Optional.ofNullable(t.getDateIncomingOther())
                        .map(dt -> new DateTime(dt)
                            .toString(DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT)))
                        .orElse(null)));
                    result.add(new ReportItemVO(t.getAmount().toString()));
                    return new ReportRowVO(result);
                })
                .collect(toList());

            if (resultRows.isEmpty()) {
                final List<ReportItemVO> emptyItems = new LinkedList<>();
                for (int jj = 0; jj++ < reportVO.getHeaders().size(); ) {
                    emptyItems.add(new ReportItemVO(""));
                }
                resultRows.add(new ReportRowVO(emptyItems));
            }
            reportVO.setRows(resultRows);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            return ex.getMessage();
        }
        return "";
    }

    private DateTime getDateTime(String date) {
        return Optional.ofNullable(date)
            .map(t -> !"".equals(t) ? DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT)
                .parseDateTime(t) : null)
            .orElse(null);
    }
}
