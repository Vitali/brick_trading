package vito.services;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vito.models.dao.ConsumptionDao;
import vito.models.dao.CustomerDao;
import vito.models.entity.Consumption;
import vito.models.entity.Customer;
import vito.models.entity.Goods;
import vito.models.vo.ConsumptionVO;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static vito.utils.Constants.CUSTOM_DATE_FORMAT;

/**
 * @author Vitaliy Ippolitov
 */
@Service
public class ConsumptionService implements IService<Consumption> {

    @Autowired
    private ConsumptionDao consumptionDao;

    @Autowired
    private CustomerDao customerDao;

    public List<ConsumptionVO> getVoData() {
        return Optional.ofNullable(getData()).orElse(new ArrayList<>(0))
                .stream()
                .map(t -> ConsumptionVO.builder()
                        .id(t.getId())
                        .customerId(
                                Optional.ofNullable(t.getCustomer()).map(Customer::getId).orElse(null))
                        .customerCaption(
                                Optional.ofNullable(t.getCustomer()).map(Customer::getCaption).orElse(null))
                        .isDebt(t.isDebt())
                        .dateConsumption(new DateTime(t.getDateConsumption()).toString(
                                DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT)))
                        .carNumber(t.getCarNumber())
                        .deliveryPrice(t.getDeliveryPrice())
                        .isCash(t.isCash())
                        .isCashless(t.isCashless())
                        .goods(Optional.ofNullable(t.getGoods())
                                .orElse(new HashSet<>()).stream().map(Goods::toGoodsVO)
                                .collect(toList()))
                        .build()
                )
                .collect(toList());
    }

    @Override
    public List<Consumption> getData() {
        final List<Consumption> result = new ArrayList<>();
        consumptionDao.findAll().forEach(result::add);
        return result;
    }

    public String addOrUpdate(Consumption consumption) {
        try {
            consumptionDao.save(consumption);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @Override
    public String delete(long id) {
        try {
            consumptionDao.delete(id);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @Deprecated
    public String find(String caption) {
        try {
            final Customer customer = customerDao.findByCaption(caption);
            final Consumption consumption = consumptionDao.findByCustomer(customer);
            return "The incoming id is: " + consumption.getId();
        } catch (Exception ex) {
            return "Incoming not found " + ex.getMessage();
        }
    }
}
