package vito.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vito.models.dao.CustomerDao;
import vito.models.entity.Customer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vitaliy Ippolitov
 */
@Service
public class CustomerService implements IService<Customer> {

    @Autowired
    private CustomerDao customerDao;

    @Override
    public List<Customer> getData() {
        List<Customer> result = new ArrayList<>();
        customerDao.findAll().forEach(result::add);
        return result;
    }

    public String addOrUpdate(Customer customer) {
        try {
            customerDao.save(customer);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @Override
    public String delete(long id) {
        try {
            customerDao.delete(id);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @Deprecated
    public String find(String caption) {
        try {
            final Customer customer = customerDao.findByCaption(caption);
            return "The customer id is: " + customer.getId();
        } catch (Exception ex) {
            return "Customer not found " + ex.getMessage();
        }
    }
}
