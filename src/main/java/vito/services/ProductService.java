package vito.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vito.models.dao.ProductDao;
import vito.models.entity.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vitaliy Ippolitov
 */
@Service
public class ProductService implements IService<Product> {

    @Autowired
    private ProductDao productDao;

    @Override
    public List<Product> getData() {
        List<Product> result = new ArrayList<>();
        productDao.findAll().forEach(result::add);
        return result;
    }

    public String addOrUpdate(Product product) {
        try {
            productDao.save(product);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @Override
    public String delete(long id) {
        try {
            productDao.delete(id);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @Deprecated
    public String find(String caption) {
        try {
            Product product = productDao.findByCaption(caption);
            return "The product id is: " + product.getId();
        } catch (Exception ex) {
            return "Product not found " + ex.getMessage();
        }
    }

    public Product findOne(Long id) {
        try {
            return productDao.findOne(id);
        } catch (Exception ex) {
            return null; // TODO throw exception
        }
    }
}
