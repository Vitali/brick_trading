package vito.services;

import vito.models.entity.BaseModel;

import java.util.List;

/**
 * @author Vitaliy Ippolitov
 */
public interface IService<ModelT extends BaseModel> {

    List<ModelT> getData();

    String delete(long id);
}