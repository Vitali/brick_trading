package vito.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vito.models.dao.IncomingOtherDao;
import vito.models.entity.IncomingOther;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vitaliy Ippolitov
 */
@Service
public class IncomingOtherService implements IService<IncomingOther> {

    @Autowired
    private IncomingOtherDao incomingOtherDao;

    @Override
    public List<IncomingOther> getData() {
        List<IncomingOther> result = new ArrayList<>();
        incomingOtherDao.findAll().forEach(result::add);
        return result;
    }

    public String addOrUpdate(IncomingOther incomingOther) {
        try {
            incomingOtherDao.save(incomingOther);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @Override
    public String delete(long id) {
        try {
            incomingOtherDao.delete(id);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @Deprecated
    public String find(String caption) {
        try {
            final IncomingOther incomingOther = incomingOtherDao.findByCaption(caption);
            return "The incomingOther id is: " + incomingOther.getId();
        } catch (Exception ex) {
            return "IncomingOther not found " + ex.getMessage();
        }
    }
}
