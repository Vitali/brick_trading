package vito.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vito.models.dao.IncomingDao;
import vito.models.dao.ProductDao;
import vito.models.entity.Incoming;
import vito.models.entity.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vitaliy Ippolitov
 */
@Service
public class IncomingService implements IService<Incoming> {

    @Autowired
    private IncomingDao incomingDao;

    @Autowired
    private ProductDao productDao;

    @Override
    public List<Incoming> getData() {
        final List<Incoming> result = new ArrayList<>();
        incomingDao.findAll().forEach(result::add);
        return result;
    }

    public String addOrUpdate(Incoming incoming) {
        try {
            incomingDao.save(incoming);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @Override
    public String delete(long id) {
        try {
            incomingDao.delete(id);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @Deprecated
    public String find(String caption) {
        try {
            final Product product = productDao.findByCaption(caption);
            final Incoming incoming = incomingDao.findByProduct(product);
            return "The incoming id is: " + incoming.getId();
        } catch (Exception ex) {
            return "Incoming not found " + ex.getMessage();
        }
    }
}
