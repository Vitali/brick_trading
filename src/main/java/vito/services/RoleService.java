package vito.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vito.models.auth.Role;
import vito.models.dao.RoleDao;

/**
 * @author Vitaliy Ippolitov
 */
@Service
public class RoleService {

    @Autowired
    private RoleDao roleDao;

    public Role findOne(Long id) {
        try {
            return roleDao.findOne(id);
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }
}
