package vito.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import vito.models.dao.UserDao;
import vito.models.entity.User;

/**
 * @author Vitaliy Ippolitov
 */
@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User client = userDao.findByUsername(username);
        if (client == null) {
            throw new UsernameNotFoundException("User with current username and password not found.");
        } else {
            try {
                return new org.springframework.security.core.userdetails.User(
                        client.getUsername(),
                        client.getPassword(),
                        client.getRoles());
            } catch (Exception repositoryProblem) {
                throw new InternalAuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
            }
        }
    }
}