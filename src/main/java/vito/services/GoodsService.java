package vito.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vito.models.dao.GoodsDao;
import vito.models.entity.Goods;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vitaliy Ippolitov
 */
@Service
public class GoodsService implements IService<Goods> {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public List<Goods> getData() {
        List<Goods> result = new ArrayList<>();
        goodsDao.findAll().forEach(result::add);
        return result;
    }

    public String addOrUpdate(Goods goods) {
        try {
            goodsDao.save(goods);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @Override
    public String delete(long id) {
        try {
            goodsDao.delete(id);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }
}
