package vito.utils;

/**
 * @author Vitaliy Ippolitov
 */
public class Constants {
    public static final String REMOVE_ERR_RU = "Не удается удалить запись! Возможно, она уже используется в других справочниках.\n";
    public static final String PARSE_DECIMAL_ERR_RU = "Ошибка при вводе денежной величины. Проверьте правильность ввода.";
    public static final String BINDING_ERR_RU = "Ошибка при считывании параметров.";
    public static final String BINDING_SERVER_ERR_RU = "Ошибка при считывании параметров связанная с преобразованием чисел.";
    public static final long USER_ROLE_ID = 2L;
    public static final String ADMIN_ROLE_CAPTION = "ADMIN";
    public static final String CUSTOM_DATE_FORMAT = "dd.MM.yyyy";
}
