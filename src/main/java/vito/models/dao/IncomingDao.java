package vito.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import vito.models.entity.Incoming;
import vito.models.entity.Product;

/**
 * @author Vitaliy Ippolitov
 */
@Transactional
public interface IncomingDao extends CrudRepository<Incoming, Long> {
    Incoming findByProduct(Product product);
}