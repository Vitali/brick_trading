package vito.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import vito.models.entity.IncomingOther;

/**
 * @author Vitaliy Ippolitov
 */
@Transactional
public interface IncomingOtherDao extends CrudRepository<IncomingOther, Long> {
    IncomingOther findByCaption(String caption);
}