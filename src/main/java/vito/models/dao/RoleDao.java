package vito.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import vito.models.auth.Role;

/**
 * @author Vitaliy Ippolitov
 */
@Transactional
public interface RoleDao extends CrudRepository<Role, Long> {
    Role findById(Long id);
}