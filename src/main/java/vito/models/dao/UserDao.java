package vito.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import vito.models.entity.User;

/**
 * @author Vitaliy Ippolitov
 */
@Transactional
public interface UserDao extends CrudRepository<User, Long> {
    User findByUsername(String username);
}