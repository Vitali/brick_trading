package vito.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import vito.models.entity.Goods;
import vito.models.entity.Incoming;
import vito.models.entity.Product;

/**
 * @author Vitaliy Ippolitov
 */
@Transactional
public interface GoodsDao extends CrudRepository<Goods, Long> {
    Incoming findByProduct(Product product);
}