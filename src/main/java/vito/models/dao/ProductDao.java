package vito.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import vito.models.entity.Product;

/**
 * @author Vitaliy Ippolitov
 */
@Transactional
public interface ProductDao extends CrudRepository<Product, Long> {
    Product findByCaption(String caption);
}