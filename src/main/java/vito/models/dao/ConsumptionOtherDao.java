package vito.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import vito.models.entity.ConsumptionOther;

/**
 * @author Vitaliy Ippolitov
 */
@Transactional
public interface ConsumptionOtherDao extends CrudRepository<ConsumptionOther, Long> {
    ConsumptionOther findByCaption(String caption);
}