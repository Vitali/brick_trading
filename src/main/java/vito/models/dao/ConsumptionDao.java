package vito.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import vito.models.entity.Consumption;
import vito.models.entity.Customer;

/**
 * @author Vitaliy Ippolitov
 */
@Transactional
public interface ConsumptionDao extends CrudRepository<Consumption, Long> {
    Consumption findByCustomer(Customer customer);
}