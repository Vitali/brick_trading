package vito.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import vito.models.entity.Customer;

/**
 * @author Vitaliy Ippolitov
 */
@Transactional
public interface CustomerDao extends CrudRepository<Customer, Long> {
    Customer findByCaption(String caption);
}