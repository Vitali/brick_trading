package vito.models.auth;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * @author Vitaliy Ippolitov
 */
@Data
@Entity
@NoArgsConstructor
@Table(name = "roles")
public class Role implements GrantedAuthority {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String role;

    public Role(Long id) {
        this.id = id;
    }

    @Override
    public String getAuthority() {
        return getRole();
    }
}