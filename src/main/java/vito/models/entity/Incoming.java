package vito.models.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Vitaliy Ippolitov
 */
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "incoming")
public class Incoming extends BaseModel {

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "products.id")
    private Product product;
    private Long volume;
    private String shipper;
    private Date dateIncoming;
    private String carNumber;
}