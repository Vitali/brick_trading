package vito.models.entity;

import lombok.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import vito.models.dto.ReportTextRowsDto;
import vito.models.vo.ReportItemVO;
import vito.models.vo.ReportRowVO;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

import static java.util.stream.Collectors.toList;
import static vito.utils.Constants.CUSTOM_DATE_FORMAT;

/**
 * @author Vitaliy Ippolitov
 */
@Entity
@Data
@Builder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "consumption")
public class Consumption extends BaseModel {

    private boolean isDebt;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "customers.id")
    private Customer customer;
    private Date dateConsumption;
    private String carNumber;
    private BigDecimal deliveryPrice;
    private boolean isCash;
    private boolean isCashless;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "goods.id")
    private Set<Goods> goods;


    public ReportRowVO toReportRowVO(List<String> simpleHeaders) {
        final List<ReportItemVO> result = new LinkedList<>();
        result.add(new ReportItemVO(
            Optional.ofNullable(this.getCustomer()).map(Customer::getCaption).orElse("")));
        result.add(new ReportItemVO(String.valueOf(this.isDebt())));
        result.add(new ReportItemVO(Optional.ofNullable(this.getDateConsumption())
            .map(dt -> new DateTime(dt)
                .toString(DateTimeFormat.forPattern(CUSTOM_DATE_FORMAT)))
            .orElse(null)));
        result.add(new ReportItemVO(this.getCarNumber()));
        result.add(new ReportItemVO(
            Optional.ofNullable(this.getDeliveryPrice()).map(BigDecimal::toString)
                .orElse("")));
        result.add(new ReportItemVO(String.valueOf(this.isCash())));
        result.add(new ReportItemVO(String.valueOf(this.isCashless())));

        final List<ReportTextRowsDto> itemRows = this.getGoods().stream()
            .map(goods -> {
                final List<String> simpleTextRow = new LinkedList<>();
                simpleTextRow.add(
                    Optional.ofNullable(goods.getProduct()).map(Product::getCaption).orElse(""));
                simpleTextRow.add(
                    Optional.ofNullable(goods.getDeliveryPrice()).orElse(BigDecimal.ZERO)
                        .toString());
                simpleTextRow.add(Optional.ofNullable(goods.getVolume()).orElse(0L).toString());
                return new ReportTextRowsDto(simpleTextRow);
            })
            .collect(toList());
        result.add(
            !itemRows.isEmpty() ? new ReportItemVO(simpleHeaders, itemRows) : new ReportItemVO("")
        );
        return new ReportRowVO(result);
    }
}