package vito.models.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Vitaliy Ippolitov
 */
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Table(name = "customers")
public class Customer extends BaseModel {
    private String caption;

    public Customer(Long id) {
        super();
        this.setId(id);
    }
}