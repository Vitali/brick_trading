package vito.models.entity;

import lombok.*;
import vito.models.vo.GoodsVO;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Vitaliy Ippolitov
 */
@Entity
@Data
@Builder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "goods")
public class Goods extends BaseModel {
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "products.id")
    private Product product;
    private BigDecimal deliveryPrice;
    private Long volume;

    public GoodsVO toGoodsVO() {
        final GoodsVO goodsVO = new GoodsVO();
        goodsVO.setId(this.getId());
        final Product product = this.getProduct();
        if (product != null) {
            goodsVO.setProductId(product.getId());
            goodsVO.setProductCaption(product.getCaption());
        }
        goodsVO.setDeliveryPrice(this.getDeliveryPrice());
        goodsVO.setVolume(this.getVolume());
        return goodsVO;
    }
}
