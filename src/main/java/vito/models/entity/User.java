package vito.models.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import vito.models.auth.Role;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Vitaliy Ippolitov
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String username;
    private String password;

    @ManyToMany
    @JoinTable(name = "users_roles",
        joinColumns = {@JoinColumn(name = "user_id")},
        inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    public User(String name, String pass, Set<Role> roles) {
        username = name;
        password = pass;
        this.roles = roles;
    }
}