package vito.models.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Vitaliy Ippolitov
 */
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Table(name = "consumption_other")
public class ConsumptionOther extends BaseModel {
    private String caption;
    private BigDecimal amount;
    private Date dateConsumptionOther;
}