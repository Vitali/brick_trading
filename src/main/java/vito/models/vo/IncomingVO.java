package vito.models.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Vitaliy Ippolitov
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IncomingVO {

    private Long id;
    private Long productId;
    private String productCaption;
    private Long volume;
    private String shipper;
    private String dateIncoming;
    private String carNumber;
}