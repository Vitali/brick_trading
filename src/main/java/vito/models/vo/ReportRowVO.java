package vito.models.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Vitaliy Ippolitov
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReportRowVO {
    private List<ReportItemVO> reportVoItems;
}
