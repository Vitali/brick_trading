package vito.models.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vito.models.entity.Goods;
import vito.models.entity.Product;

import java.math.BigDecimal;


/**
 * @author Vitaliy Ippolitov
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GoodsVO {

    private Long id;
    private Long productId;
    private String productCaption;
    private BigDecimal deliveryPrice;
    private Long volume;

    public final Goods toGoods() {
        final Product product = new Product();
        product.setId(this.getProductId());

        return Goods.builder()
            .product(product)
            .deliveryPrice(this.getDeliveryPrice())
            .volume(this.getVolume())
            .build();
    }
}