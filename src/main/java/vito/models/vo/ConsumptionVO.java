package vito.models.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Vitaliy Ippolitov
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConsumptionVO {

    private Long id;
    private Long customerId;
    private String customerCaption;
    private Boolean isDebt;
    private String dateConsumption;
    private String carNumber;
    private BigDecimal deliveryPrice;
    private Boolean isCash;
    private Boolean isCashless;
    private List<GoodsVO> goods;
}