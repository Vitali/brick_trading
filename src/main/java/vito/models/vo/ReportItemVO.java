package vito.models.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vito.models.dto.ReportTextRowsDto;

import java.util.List;

/**
 * @author Vitaliy Ippolitov
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReportItemVO {

    private String itemSimpleValue;
    private List<String> itemSimpleHeaders;
    private List<ReportTextRowsDto> itemRows;

    public ReportItemVO(String value) {
        this.itemSimpleValue = value;
    }

    public ReportItemVO(List<String> simpleHeaders, List<ReportTextRowsDto> itemRows) {
        this.itemSimpleHeaders = simpleHeaders;
        this.itemRows = itemRows;
    }
}