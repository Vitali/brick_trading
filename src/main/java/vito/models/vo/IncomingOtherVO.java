package vito.models.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author Vitaliy Ippolitov
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IncomingOtherVO {

    private Long id;
    private String caption;
    private BigDecimal amount;
    private String dateIncomingOther;
}