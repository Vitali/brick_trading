package vito.models.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Vitaliy Ippolitov
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReportVO {

    private String id;
    private Long caption;
    private String dateFrom;
    private String dateTo;

    private List<String> headers;
    private List<ReportRowVO> rows;
}