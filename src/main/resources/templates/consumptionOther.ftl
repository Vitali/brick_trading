<#import "base.ftl" as base>
<#import "/spring.ftl" as spring />
<#import "constants.ftl" as const/>
<#--@ftlvariable name="consumptionOtherVOs" type="java.util.List<vito.models.entity.ConsumptionOther>"-->
<@base.override "body">
<h1><@spring.message "header.references"/> - <@spring.message "header.references.consumption.other"/></h1>
<div class="wg-toolbar">
    <table>
        <thead>
        <tr>
            <th><@spring.message "table.header.references.caption"/></th>
            <th><@spring.message "table.header.references.date"/></th>
            <th><@spring.message "table.header.references.amount"/></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <input type="text" id="newCaption" title="">
            </td>
            <td>
                <input type="text" id="newDateConsumption" title="">
            </td>
            <td>
                <input type="text" id="newAmount" title="">
            </td>
            <td>
                <input type="button" class="btn btn-default create" value="<@spring.message "account.btn.create"/>">
            </td>
        </tr>
            <#list consumptionOtherVOs as consumptionOtherVO>
            <tr>
            <#--<td>${productVO.id}</td>-->
                <td>
                    <input type="text" title="" class="caption" data-id="${consumptionOtherVO.id?c}"
                           value="${consumptionOtherVO.caption!}"
                           readonly>
                </td>
                <td>
                    <#if consumptionOtherVO.dateConsumptionOther??>
                        <input type="text" title="" class="dateConsumptionOther"
                               value="${consumptionOtherVO.dateConsumptionOther}" readonly>
                    <#else>
                        <input type="text" title="" class="dateConsumptionOther" value="" readonly>
                    </#if>
                </td>
                <td>
                    <input type="text" title="" class="amount" value="${consumptionOtherVO.amount!}" readonly>
                </td>
                <td>
                    <input type="button" class="btn btn-default edit" value="<@spring.message "account.btn.edit"/>">
                    <input type="button" class="btn btn-default save" value="<@spring.message "account.btn.save"/>" hidden>
                    <input type="button" class="btn btn-default delete" value="<@spring.message "account.btn.remove"/>">
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
</div>
</@base.override>

<@base.override "defaultScripts">
<link rel="stylesheet" href="<@spring.url relativeUrl="/resources/css/jquery-ui.min.css"/>">
<script src="<@spring.url relativeUrl="/resources/js/lib/jquery2.min.js"/>"></script>
<script src="<@spring.url relativeUrl="/resources/js/lib/jquery-ui.min.js"/>"></script>
<script>
    $(document).ready(function () {
        $("#newDateConsumption").datepicker(DEFAULT_SETTINGS_DATE_PICKER);
        var dateConsumptionIntoTable = $(".dateConsumptionOther");
        dateConsumptionIntoTable.datepicker(DEFAULT_SETTINGS_DATE_PICKER);
        dateConsumptionIntoTable.datepicker("disable");
    });

    var $table = $('table');

    $table.on('click', '.delete', function () {
        var rowToDelete = $(this).closest('tr');
        var consumptionOthObj = {id: rowToDelete.find("input[type=text]").data('id')};
        ajaxRequest(consumptionOthObj, "consumptionOther/delete", "DELETE", function () {
            $(rowToDelete).hide(500);
        }, function (data) {
            alert('status:' + data.status + '\ntext:' + data.responseText);
        })
    });

    $table.on('click', '.edit', function () {
        $(this).closest('tr').find("input[type=text]").removeAttr('readonly');
        $(this).closest('td').find('input.save').show(250);
        $(this).closest('tr').find(".dateConsumptionOther").datepicker('enable');
    });

    $table.on('click', '.save', function () {
        var editTR = $(this).closest('tr');
        var rowInputs = editTR.find("input[type=text]");
        rowInputs.attr('readonly', true);
        var id = editTR.find(".caption").data('id');
        var caption = editTR.find(".caption").val();
        var dateConsumptionOther = editTR.find(".dateConsumptionOther").val();
        var amount = editTR.find(".amount").val().replace(',', '.').replace(/\s+/g, '') || 0;

        var incomingOthInput = {
            id: id, caption: caption, dateConsumptionOther: dateConsumptionOther, amount: amount
        };
        ajaxRequest(incomingOthInput, "consumptionOther/addOrUpdate", "POST", function () {
            $(rowInputs).slideUp();
            $(rowInputs).slideDown();
        }, function (data) {
            alert('status:' + data.status + '\ntext:' + data.responseText);
        });
        $(this).hide(50);
    });

    $table.on('click', '.create', function () {
        var caption = $("#newCaption").val();
        var dateConsumptionOther = $("#newDateConsumption").val();
        var amount = $("#newAmount").val().replace(',', '.').replace(/\s+/g, '') || 0;
        var consumptionOthInput = {
            caption: caption, dateConsumptionOther: dateConsumptionOther, amount: amount
        };
        ajaxRequest(consumptionOthInput, "consumptionOther/addOrUpdate", "POST", function () {
            window.location = window.location.href;
        }, function (data) {
            alert('status:' + data.status + '\ntext:' + data.responseText);
        })
    });

    //    TODO up ajax
    function ajaxRequest(sendData, url, type, successCallback, errorCallback) {
        $.ajax({
            url: url,
            cache: false,
            type: type,
            crossDomain: true,
            data: sendData,
            success: function (data) {
                successCallback(data);
            },
            error: function (data) {
                errorCallback(data);
            }
        });
    }
</script>
</@base.override>

<@base.override "aside">
<#--<@sideBar activeTab="ip"/>-->
</@base.override>
<@base.template />