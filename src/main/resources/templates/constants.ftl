<#import "/spring.ftl" as spring/>
<#assign MONTH_YEAR_DATE_FORMAT>MM.yyyy</#assign>
<#assign MONTH_DATE_FORMAT>MMMM</#assign>
<#assign FULL_DATE_FORMAT>dd.MM.yyyy</#assign>
<#assign FULL_DATE_FORMAT_POST>dd/mm/yyyy</#assign>
