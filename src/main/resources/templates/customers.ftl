<#import "base.ftl" as base>
<#import "/spring.ftl" as spring />
<#--@ftlvariable name="customerVOs" type="java.util.List<vito.models.entity.Customer>"-->
<@base.override "body">
<h1><@spring.message "header.references"/> - <@spring.message "header.references.customers"/></h1>
<div class="wg-toolbar">
    <table>
        <thead>
        <tr>
            <th><@spring.message "table.header.references.caption"/></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <input type="text" title="">
            </td>
            <td>
                <input type="button" class="btn btn-default create" value="<@spring.message "account.btn.create"/>">
            </td>
        </tr>
            <#list customerVOs as customerVO>
            <tr>
            <#--<td>${customerVO.id}</td>-->
                <td>
                    <input type="text" title="" data-id="${customerVO.id?c}" value="${customerVO.caption}" readonly>
                </td>
                <td>
                    <input type="button" class="btn btn-default edit" value="<@spring.message "account.btn.edit"/>">
                    <input type="button" class="btn btn-default save" value="<@spring.message "account.btn.save"/>"
                           hidden>
                    <input type="button" class="btn btn-default delete" value="<@spring.message "account.btn.remove"/>">
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
</div>
</@base.override>

<@base.override "defaultScripts">
<script src="<@spring.url relativeUrl="/resources/js/lib/jquery2.min.js"/>"></script>
<script>
    $(document).ready(function () {
    });

    var $table = $('table');

    $table.on('click', '.delete', function () {
        var rowToDelete = $(this).closest('tr');
        var customerObj = {id: rowToDelete.find("input[type=text]").data('id')};
        ajaxRequest(customerObj, "customers/delete", "DELETE", function () {
            $(rowToDelete).hide(500);
        }, function (data) {
            alert('status:' + data.status + '\ntext:' + data.responseText);
        })
    });

    $table.on('click', '.edit', function () {
        $(this).closest('tr').find("input[type=text]").removeAttr('readonly');
        $(this).closest('td').find('input.save').show(250)
    });

    $table.on('click', '.save', function () {
        var customerInput = $(this).closest('tr').find("input[type=text]");
        customerInput.attr('readonly', true);
        var customerObj = {caption: customerInput.val().substring(0, 255), id: customerInput.data('id')};

        ajaxRequest(customerObj, "customers/addOrUpdate", "POST", function () {
            $(customerInput).slideUp();
            $(customerInput).slideDown();
        }, function (data) {
            alert('status:' + data.status + '\ntext:' + data.responseText);
        });
        $(this).hide(50);
    });

    $table.on('click', '.create', function () {
        var customerObj = {caption: $(this).closest('tr').find("input[type=text]").val().substring(0, 255)};
        ajaxRequest(customerObj, "customers/addOrUpdate", "POST", function () {
            window.location = window.location.href;
        }, function (data) {
            alert('status:' + data.status + '\ntext:' + data.responseText);
        })
    });

    function ajaxRequest(sendData, url, type, successCallback, errorCallback) {
        $.ajax({
            url: url,
            cache: false,
            type: type,
            crossDomain: true, //TODO read
//            dataType: 'json',
            data: sendData,
//            data: JSON.stringify({customer: sendData}),
//            contentType: "application/json;charset=utf-8",
            success: function (data) {
                successCallback(data);
            },
            error: function (data) {
                errorCallback(data);
            }
        });
    }
</script>
</@base.override>

<@base.override "aside">
<#--<@sideBar activeTab="ip"/>-->
</@base.override>
<@base.template />