<#import "base.ftl" as base>
<#import "/spring.ftl" as spring />
<#--@ftlvariable name="requestUrl" type="java.lang.String>"-->
<#--@ftlvariable name="reportType" type="java.lang.String>"-->
<#--@ftlvariable name="reportVO" type="vito.models.vo.ReportVO"-->
<#--@ftlvariable name="customerVOs" type="java.util.List<vito.models.entity.Customer>"-->
<#--@ftlvariable name="productVOs" type="java.util.List<vito.models.entity.Product>"-->
<@base.override "body">
<h1><@spring.message "header.reports"/> -
    <#if (reportType=="TOTAL_CONSUMPTION")> <@spring.message "report.totalConsumption"/></#if>
    <#if (reportType=="DEBTS")> <@spring.message "report.debts"/></#if>
    <#if (reportType=="TOTAL_INCOMING")> <@spring.message "report.totalIncoming"/></#if>
    <#if (reportType=="BALANCE")> <@spring.message "report.balance"/></#if>
    <#if (reportType=="OTHER_CONSUMPTION")> <@spring.message "report.otherConsumption"/></#if>
    <#if (reportType=="OTHER_INCOMING")> <@spring.message "report.otherIncoming"/></#if>
</h1>
<div class="button-bar">
    <label for="inputCaption"
           <#if (reportType=="OTHER_CONSUMPTION" || reportType=="OTHER_INCOMING")>hidden</#if>>
        <@spring.message "report.dictObj"/>:</label>
    <input type="text" id="inputCaption" data-id="<#if reportVO.id??>${reportVO.id?c}</#if>"
           data-report-type="${reportType}" value="${reportVO.caption!}"
           <#if (reportType=="OTHER_CONSUMPTION" || reportType=="OTHER_INCOMING")>hidden</#if>>

    <#if (reportType!="BALANCE")>
        <label for="inputDateFrom"><@spring.message "report.dateFrom"/>:</label><input type="text" id="inputDateFrom"
                                                                                       value="${reportVO.dateFrom!}">
        <label for="inputDateTo"></label><@spring.message "report.dateTo"/>:<input type="text" id="inputDateTo"
                                                                                   value="${reportVO.dateTo!}">
    </#if>

    <input type="button" class="btn btn-default" id="reportGenerate" data-request-url="${requestUrl}"
           value="<@spring.message "account.btn.generate"/>">
</div>
<h2></h2>
<div class="wg-toolbar">
    <div id="dynamicContent">
        <#include "reportDynamicContent.ftl"/>
    </div>
    <#if customerVOs?has_content>
        <div id="customersDict" hidden>
            <table>
                <thead>
                <tr>
                    <th><@spring.message "header.references.customers"/>:</th>
                    <th <#if (reportType=="OTHER_CONSUMPTION" || reportType=="OTHER_INCOMING" ||
                    reportType=="DEBTS")>hidden</#if>>
                        <input type="button" class="btn btn-default select"
                               value="<@spring.message "account.btn.select"/>">
                    </th>
                </tr>
                </thead>
                <tbody>
                    <#list customerVOs as customerVO>
                    <tr>
                        <td><input type="text" title="" data-id="${customerVO.id?c}" value="${customerVO.caption!}"
                                   readonly>
                        </td>
                    </tr>
                    </#list>
                </tbody>
            </table>
        </div>
    </#if>

    <#if productVOs?has_content>
        <div id="goodsDict" hidden>
            <table>
                <thead>
                <tr>
                    <th><@spring.message "header.references.products"/>:</th>
                    <th  <#if (reportType=="OTHER_CONSUMPTION" || reportType=="OTHER_INCOMING" ||
                    reportType=="DEBTS")>hidden</#if>>
                        <input type="button" class="btn btn-default select"
                               value="<@spring.message "account.btn.select"/>">
                    </th>
                </tr>
                </thead>
                <tbody>
                    <#list productVOs as product>
                    <tr>
                        <td><input type="text" title="" data-id="${product.id?c}" value="${product.caption!}"
                                   readonly>
                        </td>
                    </tr>
                    </#list>
                </tbody>
            </table>
        </div>
    </#if>
</div>
</@base.override>

<@base.override "defaultScripts">
<link rel="stylesheet" href="<@spring.url relativeUrl="/resources/css/jquery-ui.min.css"/>">
<script src="<@spring.url relativeUrl="/resources/js/lib/jquery2.min.js"/>"></script>
<script src="<@spring.url relativeUrl="/resources/js/lib/jquery-ui.min.js"/>"></script>
<script>
    $(document).ready(function () {
        $("#inputDateFrom, #inputDateTo").datepicker(DEFAULT_SETTINGS_DATE_PICKER);
    });

    $("#reportGenerate").on('click', function () {
        var inputCaption = $('#inputCaption');
        var sendObj = {
            id: (inputCaption.val() == "" || inputCaption.data('id') == "") ? "" : inputCaption.data('id'),
            dateFrom: $('#inputDateFrom').val(),
            dateTo: $('#inputDateTo').val()
        };
        ajaxRequest(sendObj, $(this).data('requestUrl'), "GET", function (data) {
            $("#dynamicContent").html(data);
        }, function (errMsg) {
            alert(errMsg.responseText + "\n" + errMsg);
        })
    });

    $("#inputCaption").on('click', function () {
        var reportType = $(this).data('reportType');
        if (reportType == "TOTAL_CONSUMPTION" || reportType == "DEBTS") {
            $("#customersDict").slideDown(250);
        } else {
            if (reportType == "TOTAL_INCOMING" || reportType == "BALANCE") {
                $("#goodsDict").slideDown(250);
            }
        }
    });

    $("#customersDict, #goodsDict").on('click', ':input[type=text]', function () {
        var input = $("#inputCaption");
        var reportType = input.data('reportType');
        if (reportType == "TOTAL_CONSUMPTION" || reportType == "TOTAL_INCOMING" || reportType == "BALANCE") {
            $(this).css('background-color', $(this).css('background-color') === 'rgb(128, 128, 128)' ? 'white' : 'grey');
        } else {
            var id = $(this).data('id');
            var caption = $(this).val();
            input.data('id', id); // TODO fix all in other location to data (not attr!)
            input.val(caption);
            input.slideUp();
            input.slideDown();
            $(this).closest('div').slideUp(250);
        }
    });

    $(".select").on('click', function () {
        var ids = '';
        var captions = '';
        var tbody = $(this).closest('table').children('tbody');

        var jj;
        for (jj = 0; jj < tbody.children().size(); jj++) {
            var currentRowDOM = tbody.children()[jj];
            var currentRow = $(currentRowDOM);
            var currentInput = currentRow.find('input[type=text]');
            if (currentInput.css('background-color') == 'rgb(128, 128, 128)') {
                var id = currentInput.data('id');
                var caption = currentInput.val();
                ids = ids + ',' + id;
                captions = captions + ',' + caption;
            }
            currentInput.css('background-color', 'white');
        }

        ids = ids.substring(1, ids.length);
        captions = captions.substring(1, captions.length);
        var input = $("#inputCaption");
        input.data('id', ids); // TODO fix all in other location to data (not attr!)
        input.val(captions);
        input.slideUp();
        input.slideDown();
        $(this).closest('div').slideUp(250);
    });

    function ajaxRequest(sendData, url, type, successCallback, errorCallback) {
        $.ajax({
            url: url,
            cache: false,
            type: type,
            crossDomain: true,
            data: sendData,
            success: function (data) {
                successCallback(data);
            },
            error: function (data) {
                errorCallback(data);    // TODO made for work!
            }
        });
    }
</script>
</@base.override>

<@base.override "aside">
<#--<@sideBar activeTab="ip"/>-->
</@base.override>
<@base.template />