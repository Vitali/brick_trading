<#import "base.ftl" as base>
<#import "/spring.ftl" as spring/>
<#import "constants.ftl" as const/>
<#--@ftlvariable name="isAdminMode" type="java.lang.Boolean"-->
<@base.override "body">
<h1>Главная</h1>
<div class="wg-toolbar">
    <div id="isAdminModeDiv" hidden data-isAdminMode="<#if isAdminMode!false>true</#if>"></div>
</div>
</@base.override>

<@base.override "defaultScripts">
<link rel="stylesheet" href="<@spring.url relativeUrl="/resources/css/jquery-ui.min.css"/>">
<script src="<@spring.url relativeUrl="/resources/js/lib/jquery2.min.js"/>"></script>
<script src="<@spring.url relativeUrl="/resources/js/lib/jquery-ui.min.js"/>"></script>
<script>
    $(document).ready(function () {
        var isAdminMode = $("#isAdminModeDiv").attr("data-isAdminMode")
        if (isAdminMode == 'true') {
            $("#administrationSubMenu").show();
        }
    });
</script>
</@base.override>

<@base.override "aside">
<#--<@sideBar activeTab="ip"/>-->
</@base.override>
<@base.template />