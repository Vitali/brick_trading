<#import "/spring.ftl" as spring/>
<link rel="stylesheet" href="<@spring.url relativeUrl="/resources/css/style.css"/>">

<div class="header-menu">
    <div class="row-inner">
        <ul class="header-navigation">
            <li class="header-menu-item" data-cufon-hover=""><a href="/" title="<@spring.message "menu.references"/>">
                <@spring.message "menu.references"/></a>
                <ul class="sub-menu">
                    <li class="sub-menu-item"><a href="/products" title="Товары">Товары</a></li>
                    <li class="sub-menu-item"><a href="/customers" title="Продавцы">Продавцы</a></li>
                    <li class="sub-menu-item"><a href="/incoming" title="Поступление">Поступление</a></li>
                    <li class="sub-menu-item"><a href="/consumption" title="Реализация">Реализация</a></li>
                    <li class="sub-menu-item"><a href="/incomingOther" title="Реализация">Прочее Поступление</a></li>
                    <li class="sub-menu-item"><a href="/consumptionOther" title="Реализация">Прочее Реализация</a></li>
                </ul>
            </li>
            <li class="header-menu-item" data-cufon-hover=""><a href="/" title="<@spring.message "menu.reports"/>"><@spring.message "menu.reports"/></a>
                <ul class="sub-menu">
                    <li class="sub-menu-item"><a href="/reports/totalConsumption" title="Общий по расходам">Общий по расходам</a></li>
                    <li class="sub-menu-item"><a href="/reports/debts" title="Долги">Долги</a></li>
                    <li class="sub-menu-item"><a href="/reports/totalIncoming" title="Общий по приходу">Общий по приходу</a></li>
                    <li class="sub-menu-item"><a href="/reports/balance" title="По остатку">По остатку</a></li>
                    <li class="sub-menu-item"><a href="/reports/otherConsumption" title="Прочие Расходы">Прочие Расходы</a></li>
                    <li class="sub-menu-item"><a href="/reports/otherIncoming" title="Прочие Приходы">Прочие Приходы</a></li>
                </ul>
            </li>
            <li class="header-menu-item" data-cufon-hover=""><a href="/" title="<@spring.message "menu.office"/>"><@spring.message "menu.office"/></a>
                <ul class="sub-menu">
                    <li class="sub-menu-item" id="administrationSubMenu" style="display: none;"><a href="/users/add"
                      title="<@spring.message "menu.administration"/>"><@spring.message "menu.administration"/></a></li>
                    <li class="sub-menu-item"><a href="/logout" title="<@spring.message "menu.logout"/>"><@spring.message "menu.logout"/></a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>