<#--<#import "/spring.ftl" as spring />-->
<#--@ftlvariable name="reportVO" type="vito.models.vo.ReportVO"-->

<table>
<#if reportVO.headers?has_content>
    <thead>
    <tr>
        <#list reportVO.headers as heading>
            <th>${heading!}</th>
        </#list>
    </tr>
    </thead>
</#if>
<#if reportVO.rows?has_content>
    <tbody>
        <#list reportVO.rows as reportRowVO>
        <tr>
            <#list reportRowVO.reportItemVOList as reportItemVO>
                <td>
                    <#if reportItemVO.itemSimpleHeaders?has_content>
                        <div class="GoodsItemTable">
                            <table style="font-size: 12px;">
                                <thead>
                                <tr>
                                    <#list reportItemVO.itemSimpleHeaders as simpleHeading>
                                        <th>${simpleHeading!}</th>
                                    </#list>
                                </tr>
                                </thead>
                                <tbody>
                                    <#list reportItemVO.itemRows as reportTextRow>
                                    <tr>
                                        <#list reportTextRow.simpleTextRow as simpleTextRow>
                                            <td>
                                                <input type="text" title="" value="${simpleTextRow!}"
                                                       style="width: 80px;">
                                            </td>
                                        </#list>
                                    </tr>
                                    </#list>
                                </tbody>
                            </table>
                        </div>
                    <#else>
                        <input type="text" value="${reportItemVO.itemSimpleValue!}" style="width: 100px;" readonly
                               title="">
                    </#if>
                </td>
            </#list>
        </tr>
        </#list>
    </tbody>
</#if>
</table>
