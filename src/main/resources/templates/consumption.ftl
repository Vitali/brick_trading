<#import "base.ftl" as base>
<#import "/spring.ftl" as spring/>
<#import "constants.ftl" as const/>
<#--@ftlvariable name="consumptionVOs" type="java.util.List<vito.models.vo.ConsumptionVO>"-->
<#--@ftlvariable name="customerVOs" type="java.util.List<vito.models.entity.Customer>"-->
<#--@ftlvariable name="productVOs" type="java.util.List<vito.models.entity.Product>"-->
<#--@ftlvariable name="goodsVOs" type="java.util.List<vito.models.entity.Goods>"-->
<#--@ftlvariable name="isAdminMode" type="java.lang.Boolean"-->
<@base.override "body">
<h1><@spring.message "header.references"/> - <@spring.message "header.references.consumption"/></h1>
<div class="wg-toolbar">
    <div id="isAdminModeDiv" hidden data-isAdminMode="<#if isAdminMode!false>true</#if>"></div>
    <table>
        <thead style="font-size: 12px;">
        <tr>
            <th><@spring.message "consumption.customer"/></th>
            <th><@spring.message "consumption.isDebt"/></th>
            <th><@spring.message "consumption.date"/></th>
            <th><@spring.message "consumption.carNumber"/></th>
            <th><@spring.message "consumption.deliveryPrice"/></th>
            <th><@spring.message "consumption.isCash"/></th>
            <th><@spring.message "consumption.isCashless"/></th>
            <th><@spring.message "consumption.goods"/></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <input type="text" id="newCustomer" data-customer-id="" title="">
            </td>
            <td>
                <input type="checkbox" title="newIsDebt">
            </td>
            <td>
                <input type="text" id="newDateConsumption" title="" style="width: 100px;">
            </td>
            <td>
                <input type="text" title="newCarNumber" style="width: 100px;">
            </td>
            <td>
                <input type="text" title="newDeliveryPrice" style="width: 100px;">
            </td>
            <td>
                <input type="checkbox" title="newIsCash">
            </td>
            <td>
                <input type="checkbox" title="newIsCashless">
            </td>
            <td>
                <div class="newGoods">
                    <table style="font-size: 12px;">
                        <thead class="goodsTableHeader">
                        <tr>
                            <th><@spring.message "consumption.goods.product"/></th>
                            <th><@spring.message "consumption.goods.deliveryPrice"/></th>
                            <th><@spring.message "consumption.goods.volume"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <input type="text" class="goodsProduct edited" title="" data-id=""
                                       style="width: 80px;">
                            </td>
                            <td>
                                <input type="text" class="goodsDeliveryPrice" title=""
                                       value=""
                                       style="width: 80px;">
                            </td>
                            <td>
                                <input type="text" class="goodsVolume" title="" value=""
                                       style="width: 80px;">
                            </td>
                            <td>
                                <input type="button" class="goodsRemoveRow" title="" value="-"
                                       style="width: 30px;">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </td>
            <td>
                <input type="button" class="btn btn-default create" value="<@spring.message "account.btn.create"/>">
            </td>
        </tr>
            <#if consumptionVOs??>
                <#list consumptionVOs as consumptionVO>
                <tr>
                <#--<td>${consumptionVO.id}</td>-->
                    <td>
                        <#if consumptionVO.customerId??>
                            <input type="text" title="" class="customer"
                                   data-consumption-id="${consumptionVO.id?c}"
                                   data-customer-id="${consumptionVO.customerId!}"
                                   value="${consumptionVO.customerCaption!}" readonly>
                        <#else>
                            <input type="text" title="" class="customer"
                                   data-consumption-id="${consumptionVO.id?c}" data-product-id="" readonly>
                        </#if>
                    </td>
                    <td>
                        <#if consumptionVO.isDebt>
                            <input type="checkbox" class="isDebt" checked readonly title="">
                        <#else>
                            <input type="checkbox" class="isDebt" readonly title="">
                        </#if>
                    </td>
                    <td>
                        <#if consumptionVO.dateConsumption??>
                            <input type="text" title="" class="dateConsumption input-date" style="width: 100px;"
                                   value="${consumptionVO.dateConsumption}" readonly>
                        <#else>
                            <input type="text" title="" class="dateConsumption input-date" value="" readonly>
                        </#if>
                    </td>
                    <td>
                        <input type="text" title="" class="carNumber" value="${consumptionVO.carNumber!}"
                               style="width: 100px;" readonly>
                    </td>
                    <td>
                        <input type="text" title="" class="deliveryPrice" value="${consumptionVO.deliveryPrice!}"
                               style="width: 100px;" readonly>
                    </td>
                    <td>
                        <#if consumptionVO.isCash>
                            <input type="checkbox" class="isCash" checked readonly title="">
                        <#else>
                            <input type="checkbox" class="isCash" readonly title="">
                        </#if>
                    </td>
                    <td>
                        <#if consumptionVO.isCashless>
                            <input type="checkbox" class="isCashless" checked readonly title="">
                        <#else>
                            <input type="checkbox" class="isCashless" readonly title="">
                        </#if>
                    </td>
                    <td>
                        <div class="goods">
                            <table style="font-size: 12px;">
                                <#if consumptionVO.goods?has_content>
                                    <thead class="goodsTableHeader">
                                    <tr>
                                        <th><@spring.message "consumption.goods.product"/></th>
                                        <th><@spring.message "consumption.goods.deliveryPrice"/></th>
                                        <th><@spring.message "consumption.goods.volume"/></th>
                                    </tr>
                                    </thead>
                                <#else>
                                    <thead hidden class="goodsTableHeader">
                                    <tr>
                                        <th><@spring.message "consumption.goods.product"/></th>
                                        <th><@spring.message "consumption.goods.deliveryPrice"/></th>
                                        <th><@spring.message "consumption.goods.volume"/></th>
                                    </tr>
                                    </thead>
                                </#if>
                                <tbody>
                                    <#list consumptionVO.goods as goodsVO>
                                    <tr>
                                        <td><#if goodsVO.productId??>
                                            <input type="text" class="goodsProduct" title="" data-id="${goodsVO.id?c}"
                                                   data-product-id="${goodsVO.productId?c}"
                                                   value="${goodsVO.productCaption!}" style="width: 80px;" readonly>
                                        <#else>
                                            <input type="text" class="goodsProduct" title="" data-id="${goodsVO.id?c}"
                                                   style="width: 80px;" readonly>
                                        </#if>
                                        </td>
                                        <td>
                                            <input type="text" class="goodsDeliveryPrice" title=""
                                                   value="${goodsVO.deliveryPrice!}"
                                                   style="width: 80px;" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="goodsVolume" title="" value="${goodsVO.volume!}"
                                                   style="width: 80px;" readonly>
                                        </td>
                                        <td>
                                            <input type="button" class="goodsRemoveRow" title="" value="-"
                                                   style="width: 30px;" readonly>
                                        </td>
                                    </tr>
                                    </#list>
                                </tbody>
                            </table>
                        </div>
                    </td>
                    <td>
                        <input type="button" class="btn btn-default edit" value="<@spring.message "account.btn.edit"/>">
                        <input type="button" class="btn btn-default save" value="<@spring.message "account.btn.save"/>" hidden>
                        <input type="button" class="btn btn-default delete" value="<@spring.message "account.btn.remove"/>">
                        <input type="button" class="btn goodsBtnAdd" value="+goods" hidden>
                    </td>
                </tr>
                </#list>
            </#if>
        </tbody>
    </table>

    <div id="customersDict" style="display: none;">
        <table>
            <thead>
            <tr>
                <th>Customers</th>
            </tr>
            </thead>
            <tbody>
                <#list customerVOs as customerVO>
                <tr>
                    <td>
                        <input type="text" title="" data-id="${customerVO.id?c}" value="${customerVO.caption!}"
                               readonly>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>

    <div id="productsDict" style="display: none;">
        <table>
            <thead>
            <tr>
                <th>Products</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <#list productVOs as product>
                <tr>
                    <td>
                        <input type="text" title="" data-id="${product.id?c}" value="${product.caption!}" readonly>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>
</div>
</@base.override>

<@base.override "defaultScripts">
<link rel="stylesheet" href="<@spring.url relativeUrl="/resources/css/jquery-ui.min.css"/>">
<script src="<@spring.url relativeUrl="/resources/js/lib/jquery2.min.js"/>"></script>
<script src="<@spring.url relativeUrl="/resources/js/lib/jquery-ui.min.js"/>"></script>
<script>
    $(document).ready(function () {
        $("#newDateConsumption").datepicker(DEFAULT_SETTINGS_DATE_PICKER);
        var dateConsumptionIntoTable = $(".dateConsumption");
        dateConsumptionIntoTable.datepicker(DEFAULT_SETTINGS_DATE_PICKER);
        dateConsumptionIntoTable.datepicker("disable");
    });

    var $table = $('table');

    $table.on('click', '.delete', function () {
        var rowToDelete = $(this).closest('tr');
        var consumptionObj = {
            id: rowToDelete.find(".customer").data('consumptionId'),
            customerId: 0,
            isDebt: false,
            dateConsumption: "1.1.1",
            carNumber: 0,
            deliveryPrice: 0,
            isCash: false,
            isCashless: false,
            goods: []
        };
        ajaxRequest(consumptionObj, "consumption/delete", "DELETE", function () {
            $(rowToDelete).hide(500);
        }, function (data) {
            if (data.status == 200) { // TODO apply a good solution
                $(rowToDelete).hide(500);
            } else {
                if (data.status == 403) {
                    alert('Недостаточно прав для выполнения операции, обратитесь к администратору');
                } else {
                    alert('status:' + data.status + '\ntext:' + data.responseText);
                }
            }
        })
    });

    $table.on('click', '.edit', function () {
        var closestTr = $(this).closest('tr');
        closestTr.find("input[type=text]:not(.goodsProduct)").removeAttr('readonly');
        closestTr.find("input[type=text].goodsProduct").addClass('edited');
        closestTr.find(".dateIncoming").datepicker('enable');
        closestTr.find("input.goodsBtnAdd").show(250);
        closestTr.find(".goodsTableHeader").show(250);
        $(this).closest('td').find('input.save').show(250);
    });

    $table.on('click', '.save', function () {
        var editTR = $(this).closest('tr');
        var consumptionInputs = editTR.find("input[type=text]");
        var consumptionId = editTR.find(".customer").data('consumptionId');
        var customerId = editTR.find(".customer").data('customerId');
        var isDebt = editTR.find(".isDebt").is(":checked");
        var dateConsumption = editTR.find(".dateConsumption").val();
        var carNumber = editTR.find(".carNumber").val();
        var deliveryPrice = editTR.find(".deliveryPrice").val().replace(',', '.').replace(/\s+/g, '');
        var isCash = editTR.find(".isCash").is(":checked");
        var isCashless = editTR.find(".isCashless").is(":checked");
        var goodsTBody = editTR.find(".goods").find("tbody");
        var goods = [];
        var jj;
        for (jj = 0; jj < goodsTBody.children().size(); jj++) {
            var currentRowDOM = goodsTBody.children()[jj];
            var currentRow = $(currentRowDOM);
            if (currentRow.attr("style") != "display: none;") { // TODO simply
                var id = currentRow.find(".goodsProduct").data('id');
                var productId = currentRow.find(".goodsProduct").data('productId');
                var goodsDeliveryPrice = currentRow.find(".goodsDeliveryPrice").val().replace(',', '.').replace(/\s+/g, '');
                var volume = currentRow.find(".goodsVolume").val().replace(',', '.').replace(/\s+/g, '');
                goods.push({id: id, productId: productId, deliveryPrice: goodsDeliveryPrice, volume: volume});
            }
        }
        var consumptionObj = {
            id: consumptionId, customerId: customerId, isDebt: isDebt, dateConsumption: dateConsumption,
            carNumber: carNumber, deliveryPrice: deliveryPrice, isCash: isCash, isCashless: isCashless,
            goods: goods
        };

        var editedInputGoods = $table.find(':input.goodsProduct');
        editedInputGoods.removeClass('edited');

        ajaxRequest(consumptionObj, "consumption/addOrUpdate", "POST", function () {
            $(consumptionInputs).slideUp();
            $(consumptionInputs).slideDown();
        }, function (data) {
            if (data.status == 200) { // TODO apply a good solution
                $(consumptionInputs).slideUp();
                $(consumptionInputs).slideDown();
            }
            else {
                alert('status:' + data.status + '\ntext:' + data.responseText);
            }
        });
        editTR.find(".dateIncoming").datepicker('disable');
        $(this).hide(50);
        $(this).closest('tr').find("input.goodsBtnAdd").hide(50);
    });

    $table.on('click', '.create', function () {
        var newFieldsRow = $(this).closest('tr');
        var customerId = $("#newCustomer").data('customerId');
        var isDebt = newFieldsRow.find("input[title='newIsDebt']").is(":checked");
        var dateConsumption = $("#newDateConsumption").val();
        var carNumber = newFieldsRow.find("input[title='newCarNumber']").val();
        var deliveryPrice = newFieldsRow.find("input[title='newDeliveryPrice']").val();
        var isCash = newFieldsRow.find("input[title='newIsCash']").is(":checked");
        var isCashless = newFieldsRow.find("input[title='newIsCashless']").is(":checked");

        var goodsTBody = newFieldsRow.find(".newGoods").find("tbody");
        var goods = [];
        var jj;
        for (jj = 0; jj < goodsTBody.children().size(); jj++) {
            var currentRowDOM = goodsTBody.children()[jj];
            var currentRow = $(currentRowDOM);
            if (currentRow.attr("style") != "display: none;") { // TODO simply
                var id = currentRow.find(".goodsProduct").data('id');
                var productId = currentRow.find(".goodsProduct").data('productId');
                var goodsDeliveryPrice = currentRow.find(".goodsDeliveryPrice").val().replace(',', '.').replace(/\s+/g, '');
                var volume = currentRow.find(".goodsVolume").val().replace(',', '.').replace(/\s+/g, '');
                goods.push({id: id, productId: productId, deliveryPrice: goodsDeliveryPrice, volume: volume});
            }
        }

        var consumptionObj = {
            customerId: customerId, isDebt: isDebt, dateConsumption: dateConsumption, carNumber: carNumber
            , deliveryPrice: deliveryPrice, isCash: isCash, isCashless: isCashless, goods: goods
        };
        ajaxRequest(consumptionObj, "consumption/addOrUpdate", "POST", function () {
            window.location = window.location.href;
        }, function (data) {
            if (data.status == 200) { // TODO apply a good solution
                window.location = window.location.href;
            } else {
                alert('status:' + data.status + '\ntext:' + data.responseText);
            }
        });
    });

    $table.on('click', ':input:not([readonly]).customer', function () {
        $("#customersDict").slideDown(250);
    });

    $table.on('click', ':input#newCustomer', function () {
        $("#customersDict").slideDown(250);
    });

    $table.on('click', ':input.goodsProduct.edited', function () {
        $table.find(':input.goodsProduct').removeAttr('data-last-selected-goods');
        $(this).attr("data-last-selected-goods", "")
        $("#productsDict").slideDown(250);
    });

    $table.on('click', '.goodsBtnAdd', function () {
        var localTBody = $(this).closest('tr').find(".goods").find("tbody");
        localTBody.append('<tr><td><input type="text" class="goodsProduct" title="" data-id="" style="width: 80px;"></td><td><input type="text" class="goodsDeliveryPrice" title=""style="width: 80px;"></td><td><input type="text" class="goodsVolume" title=""style="width: 80px;"></td></tr>');
    });

    $table.on('click', '.goodsRemoveRow', function () {
        $(this).closest('tr').hide();
    });

    $("#customersDict").on('click', ':input', function () {
        var customerId = $(this).data('id');
        var customerCaption = $(this).val();
        var inputCustomer = $table.find(':input:not([readonly]).customer');
        if (inputCustomer.length == 0) {
            inputCustomer = $("#newCustomer");
        }
        inputCustomer.attr('data-customer-id', customerId);
        inputCustomer.val(customerCaption);
        inputCustomer.slideUp();
        inputCustomer.slideDown();
        $("#customersDict").slideUp(250);
    });

    $("#productsDict").on('click', ':input', function () {
        var productId = $(this).data('id');
        var productCaption = $(this).val();
        var inputProduct = $table.find(':input.goodsProduct[data-last-selected-goods]');
        var editedInputGoods = $table.find(':input.goodsProduct');
        editedInputGoods.removeAttr('data-last-selected-goods');
        if (inputProduct.length == 0) {
            //TODO inputCustomer = $("#newGoodsProduct");
        }
        inputProduct.attr('data-product-id', productId);
        inputProduct.val(productCaption);
        inputProduct.slideUp();
        inputProduct.slideDown();
        $("#productsDict").slideUp(250);
    });

    function ajaxRequest(sendData, url, type, successCallback, errorCallback) {
        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: url,
            cache: false,
            type: type,
            dataType: 'json',
            data: JSON.stringify(sendData),
            crossDomain: true,
            success: function (data) {
                successCallback(data);
            },
            error: function (data) {
                errorCallback(data);
            }
        });
    }
</script>
</@base.override>

<@base.override "aside">
<#--<@sideBar activeTab="ip"/>-->
</@base.override>
<@base.template />