<#import "base.ftl" as base>
<#import "/spring.ftl" as spring />
<#--@ftlvariable name="productVOs" type="java.util.List<vito.models.entity.Product>"-->
<@base.override "body">
<h1><@spring.message "header.references"/> - <@spring.message "header.references.products"/></h1>
<div class="wg-toolbar">
    <table>
        <thead>
        <tr>
            <th><@spring.message "table.header.references.caption"/></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <input type="text" title="">
            </td>
            <td>
                <input type="button" class="btn btn-default create" value="<@spring.message "account.btn.create"/>">
            </td>
        </tr>
            <#list productVOs as productVO>
            <tr>
            <#--<td>${productVO.id}</td>-->
                <td>
                    <input type="text" title="" data-id="${productVO.id?c}" value="${productVO.caption!}" readonly>
                </td>
                <td>
                    <input type="button" class="btn btn-default edit" value="<@spring.message "account.btn.edit"/>">
                    <input type="button" class="btn btn-default save" value="<@spring.message "account.btn.save"/>"
                           hidden>
                    <input type="button" class="btn btn-default delete" value="<@spring.message "account.btn.remove"/>">
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
</div>
</@base.override>

<@base.override "defaultScripts">
<script src="<@spring.url relativeUrl="/resources/js/lib/jquery2.min.js"/>"></script>
<script>
    $(document).ready(function () {
    });

    var $table = $('table');

    $table.on('click', '.delete', function () {
        var rowToDelete = $(this).closest('tr');
        var productObj = {id: rowToDelete.find("input[type=text]").data('id')};
        ajaxRequest(productObj, "products/delete", "DELETE", function () {
            $(rowToDelete).hide(500);
        }, function (data) {
            alert('status:' + data.status + '\ntext:' + data.responseText);
        })
    });

    $table.on('click', '.edit', function () {
        $(this).closest('tr').find("input[type=text]").removeAttr('readonly');
        $(this).closest('td').find('input.save').show(250)
    });

    $table.on('click', '.save', function () {
        var productInput = $(this).closest('tr').find("input[type=text]");
        productInput.attr('readonly', true);
        var productObj = {caption: productInput.val().substring(0, 255), id: productInput.data('id')};

        ajaxRequest(productObj, "products/addOrUpdate", "POST", function () {
            $(productInput).slideUp();
            $(productInput).slideDown();
        }, function (data) {
            alert('status:' + data.status + '\ntext:' + data.responseText);
        });
        $(this).hide(50);
    });

    $table.on('click', '.create', function () {
        var productObj = {caption: $(this).closest('tr').find("input[type=text]").val().substring(0, 255)};
        ajaxRequest(productObj, "products/addOrUpdate", "POST", function () {
            window.location = window.location.href;
        }, function (data) {
            alert('status:' + data.status + '\ntext:' + data.responseText);
        })
    });

    function ajaxRequest(sendData, url, type, successCallback, errorCallback) {
        $.ajax({
            url: url,
            cache: false,
            type: type,
            crossDomain: true,
            data: sendData,
            success: function (data) {
                successCallback(data);
            },
            error: function (data) {
                errorCallback(data);
            }
        });
    }
</script>
</@base.override>

<@base.override "aside">
<#--<@sideBar activeTab="ip"/>-->
</@base.override>
<@base.template />