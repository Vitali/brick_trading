<#ftl encoding="UTF-8">
<#setting locale="ru">
<#--HTML BASE TEMPLATE -->

<#import "/spring.ftl" as spring/>

<#global html_block = {}>

<#macro override name>
    <#local  html_tmp><#nested></#local>
    <#local  html_tmp_inherited=html_block[name]!""/>
    <#global html_block = html_block + {name:(html_tmp+html_tmp_inherited)}>
</#macro>

<#macro block name>
${html_block[name]!""}
</#macro>

<#macro template>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Brick Trading</title>
</head>

<body>
<div>
    <div class="header">
        <#include "header.ftl"/>
    </div>
    <div class="content">
        <div class="row-inner ">
        <#--Side menu-->
            <div class="aside">
                <@block "aside" />
                <#--TODO macros with one parameter (active link)-->
            </div>
        <#--Content-->
            <div class="page-content">
                <@block "body"/>
            </div>
        </div>
    </div>
<#--<div class="footer">-->
<#--<#include "footer.ftl">-->
<#--</div>-->
</div>
<script>
    var DEFAULT_SETTINGS_DATE_PICKER = {
        closeText: "Закрыть",
        prevText: "&#x3C;Пред",
        nextText: "След&#x3E;",
        currentText: "Сегодня",
        monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
            "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        monthNamesShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн",
            "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
        dayNames: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
        dayNamesShort: ["вск", "пнд", "втр", "срд", "чтв", "птн", "сбт"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        weekHeader: "Нед",
        dateFormat: "dd.mm.yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ""
    };
</script>
    <@block "defaultScripts"/>
</body>
</html>
</#macro>