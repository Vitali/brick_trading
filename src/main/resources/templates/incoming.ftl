<#import "base.ftl" as base>
<#import "/spring.ftl" as spring/>
<#import "constants.ftl" as const/>
<#--@ftlvariable name="incomingVOs" type="java.util.List<vito.models.vo.IncomingVO>"-->
<#--@ftlvariable name="productVOs" type="java.util.List<vito.models.entity.Product>"-->
<#--@ftlvariable name="isAdminMode" type="java.lang.Boolean"-->
<#--TODO ALL id computer friendly number-->
<@base.override "body">
<h1><@spring.message "header.references"/> - <@spring.message "header.references.incoming"/></h1>
<div class="wg-toolbar">
    <table>
        <thead>
        <tr>
            <th><@spring.message "incoming.product"/></th>
            <th><@spring.message "incoming.volume"/></th>
            <th><@spring.message "incoming.shipper"/></th>
            <th><@spring.message "incoming.dateIncoming"/></th>
            <th><@spring.message "incoming.carNumber"/></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <input type="text" id="newProduct" data-product-id="">
            </td>
            <td>
                <input type="text" title="newVolume">
            </td>
            <td>
                <input type="text" title="newShipper">
            </td>
            <td>
                <input type="text" id="newDateIncoming" title="">
            </td>
            <td>
                <input type="text" title="newCarNumber">
            </td>
            <td>
                <input type="button" class="btn btn-default create" value="<@spring.message "account.btn.create"/>">
            </td>
        </tr>
            <#if incomingVOs??>
                <#list incomingVOs as incomingVO>
                <tr>
                <#--<td>${incomingVO.id}</td>-->
                    <td>
                        <#if incomingVO.productId??>
                            <input type="text" title="" class="product"
                                   data-incoming-id="${incomingVO.id?c}"
                                   data-product-id="${incomingVO.productId?c}"
                                   value="${incomingVO.productCaption!}" readonly>
                        <#else>
                            <input type="text" title="" class="product"
                                   data-incoming-id="${incomingVO.id?c}" data-product-id="" readonly>
                        </#if>
                    </td>
                    <td>
                        <input type="text" title="" class="volume" value="${incomingVO.volume!}" readonly>
                    </td>
                    <td>
                        <input type="text" title="" class="shipper" value="${incomingVO.shipper!}" readonly>
                    </td>
                    <td>
                        <#if incomingVO.dateIncoming??>
                            <input type="text" title="" class="dateIncoming"
                                   value="${incomingVO.dateIncoming}" readonly>
                        <#else>
                            <input type="text" title="" class="dateIncoming" value="" readonly>
                        </#if>
                    </td>
                    <td>
                        <input type="text" title="" class="carNumber" value="${incomingVO.carNumber!}" readonly>
                    </td>
                    <#if isAdminMode!false>
                        <td>
                            <input type="button" class="btn btn-default edit"
                                   value="<@spring.message "account.btn.edit"/>">
                            <input type="button" class="btn btn-default save"
                                   value="<@spring.message "account.btn.save"/>" hidden>
                            <input type="button" class="btn btn-default delete"
                                   value="<@spring.message "account.btn.remove"/>">
                        </td>
                    </#if>
                </tr>
                </#list>
            </#if>
        </tbody>
    </table>

    <div id="productsDict" style="display: none;">
        <p>References-Product:</p>
        <table>
            <thead>
            <tr>
                <th>Caption</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <#list productVOs as productVO>
                <tr>
                    <td>
                        <input type="text" title="" data-id="${productVO.id?c}" value="${productVO.caption!}" readonly>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>
</div>
</@base.override>

<@base.override "defaultScripts">
<link rel="stylesheet" href="<@spring.url relativeUrl="/resources/css/jquery-ui.min.css"/>">
<script src="<@spring.url relativeUrl="/resources/js/lib/jquery2.min.js"/>"></script>
<script src="<@spring.url relativeUrl="/resources/js/lib/jquery-ui.min.js"/>"></script>
<#--TODO most sightly calendar http://rtsinani.github.io/jquery-datepicker-skins/-->
<script>
    $(document).ready(function () {
        $("#newDateIncoming").datepicker(DEFAULT_SETTINGS_DATE_PICKER);
        var dateIncomingIntoTable = $(".dateIncoming");
        dateIncomingIntoTable.datepicker(DEFAULT_SETTINGS_DATE_PICKER);
        dateIncomingIntoTable.datepicker("disable");
    });

    var $table = $('table');

    $table.on('click', '.delete', function () {
        var rowToDelete = $(this).closest('tr');
        var incomingObj = {id: rowToDelete.find(".product").data('incomingId')};
        ajaxRequest(incomingObj, "incoming/delete", "DELETE", function () {
            $(rowToDelete).hide(500);
        }, function (data) {
            if (data.status == 403) {
                alert('Недостаточно прав для выполнения операции, обратитесь к администратору');
            } else {
                alert('status:' + data.status + '\ntext:' + data.responseText);
            }
        })
    });

    $table.on('click', '.edit', function () {
        $(this).closest('tr').find("input[type=text]").removeAttr('readonly');
        $(this).closest('td').find('input.save').show(250);
        $(this).closest('tr').find(".dateIncoming").datepicker('enable');
    });

    $table.on('click', '.save', function () {
        var editTR = $(this).closest('tr');
        var incomingInputs = editTR.find("input[type=text]");
        incomingInputs.attr('readonly', true);
        var incomingId = editTR.find(".product").data('incomingId');
        var productId = editTR.find(".product").data('productId');
        var volume = editTR.find(".volume").val().replace(',', '.').replace(/\s+/g, '') || 0;
        var shipper = editTR.find(".shipper").val();
        var dateIncoming = editTR.find(".dateIncoming").val();
        var carNumber = editTR.find(".carNumber").val();
        var incomingObj = {
            id: incomingId, productId: productId, volume: volume, shipper: shipper,
            dateIncoming: dateIncoming, carNumber: carNumber
        };

        ajaxRequest(incomingObj, "incoming/addOrUpdate", "POST", function () {
            $(incomingInputs).slideUp();
            $(incomingInputs).slideDown();
        }, function (data) {
            if (data.status == 403) {
                alert('Недостаточно прав для выполнения операции, обратитесь к администратору');
            } else {
                alert('status:' + data.status + '\ntext:' + data.responseText);
            }
        });
        editTR.find(".dateIncoming").datepicker('disable');
        $(this).hide(50);
    });

    $table.on('click', '.create', function () {
        var newFieldsRow = $(this).closest('tr');
        var productId = $("#newProduct").data('productId');
        var volume = newFieldsRow.find("input[title='newVolume']").val() || 0;
        var shipper = newFieldsRow.find("input[title='newShipper']").val();
        var dateIncoming = $("#newDateIncoming").val();
        var carNumber = newFieldsRow.find("input[title='newCarNumber']").val();
        var incomingObj = dateIncoming != "" ? {
            productId: productId, volume: volume, shipper: shipper,
            dateIncoming: dateIncoming, carNumber: carNumber
        } : {
            productId: productId, volume: volume, shipper: shipper,
            carNumber: carNumber
        };// TODO delete trinare
        ajaxRequest(incomingObj, "incoming/addOrUpdate", "POST", function () {
            window.location = window.location.href;
        }, function (data) {
            alert('status:' + data.status + '\ntext:' + data.responseText);
        })
    });

    $table.on('click', ':input:not([readonly]).product', function () {
        $("#productsDict").slideDown(250);
    });

    $("#newProduct").on('click', function () {
        $("#productsDict").slideDown(250);
    });

    $("#productsDict").on('click', ':input', function () {
        var productId = $(this).data('id');
        var productCaption = $(this).val();
        var inputProduct = $table.find(':input:not([readonly]).product');
        if (inputProduct.length == 0) {
            inputProduct = $("#newProduct");
        }
        inputProduct.attr('data-product-id', productId);
        inputProduct.val(productCaption);
        inputProduct.slideUp();
        inputProduct.slideDown();
        $("#productsDict").slideUp(250);
    });

    function ajaxRequest(sendData, url, type, successCallback, errorCallback) {
        $.ajax({
            url: url,
            cache: false,
            type: type,
            crossDomain: true,
            data: sendData,
            success: function (data) {
                successCallback(data);
            },
            error: function (data) {
                errorCallback(data);
            }
        });
    }
</script>
</@base.override>

<@base.override "aside">
<#--<@sideBar activeTab="ip"/>-->
</@base.override>
<@base.template />