/**
 * Created by blvp on 09.06.15.
 */
var MainPage = extend.Page({
    delegatedEvents: {
        "click #js-services-click": "redirect",
        "click #js-promise-pay": "redirect",
        "click #js-balance-refresh": "refreshBalance",
        "keyup #js-make-payment-amount": "checkForNumbers",
        "click .js-url": "openPage",
        "click .js-close-showBill": "closeShowBill",
        "click #js-logout": "redirect",
        "click #js-make-payment-submit": "makePaymentSubmit",
        "click #js-make-payment-upsale": "makeLightPaymentSubmit",
        "click .close-section": "closeSection",
        "click .js-cancel": 'cancelPopup',
        'click .js-invite-close': 'closeInviteShowBill',
        "click #js-btn-show_confirm_upsale_form": 'showConfirmUpsaleForm',
        "click #js-btn-make_upsale_jump": 'makeUpsaleJump'
    },
    onRefresh: function () {
        cufonReplace();
    },
    placeServiceManageLink: function () {
        if (this.emptyDiscounts) {
            this.servicesBlock.append($('#js-services-manage'));
        }
    },
    components: {
        "balanceInfo": "#js-account-info",
        "makePaymentForm": "#js-make-payment-form",
        "makeLightPaymentForm": "#js-make-light-payment-form",
        "makePaymentPhone": "#js-make-payment-phone",
        "recommendedBlock": "#js-recommended-block",
        "servicesBlock": '#js-services-block',
        "tariffBlock": '#js-tariff-block'
    },
    openPage: function (evt) {
        var url = evt.$el.data('href');
        window.open(url);
    },

    closeInviteShowBill: function (evt) {
        evt.preventDefault();
        evt.$el.closest('.row').hide();
    },
    redirect: function (evt) {
        window.location = evt.$el.data('href');
    },
    makePaymentSubmit: function () {
        $.ajax({
            cache: false,
            url: "/payment/makePayment",
            type: "POST"
        });
    },
    makeLightPaymentSubmit: function () {
        $.ajax({
            cache: false,
            url: "/lightPayment/makePayment",
            type: "POST"
        });
    },
    refreshBalance: function (evt) {
        $('#js-balance-refresh').addClass('load-animation');
        $.get("/getFreshBalance", {"_": $.now()})
            .done(function (data) {
                $('#js-services-manage').remove();
                evt.that.balanceInfo.html(data);
                evt.that.placeServiceManageLink();
                evt.that.refresh();
            });
    },
    checkForNumbers: function (evt) {
        var amount = evt.$el.val();
        var numberWithoutLeadingZeroes = /^([1-9])(\d*)$/;
        if (!numberWithoutLeadingZeroes.test(amount)) {
            evt.$el.val("");
        }
    }
    ,
    closeShowBill: function (evt) {
        var showBillIdSel = evt.$el.data('rel');
        $.post("/updateShowBillStatus")
            .done(function () {
                $(showBillIdSel).hide();
            })
            .fail(function () {
                console.log("на сервере возникла некоторая ошибка!");
            });
    },
    loadBlocks: function () {
        this.loadRecommendedBlock();
        this.loadTariffAndBalanceBlock();
        this.loadDiscountsBlock();
    },
    loadRecommendedBlock: function () {
        var that = this;
        $.get("main/recommendedBlock", {"_": $.now()})
            .done(function (data) {
                that.recommendedBlock.html(that.recommendedBlockTemplateFn({
                    items: data
                }));
                that.refresh();
            })
            .error(function () {
                that.recommendedBlock.html("<div class='recommended-error'>" + JS_DATA["errorMainpageLoading"] + "</div>");
            });
    },
    loadTariffAndBalanceBlock: function () {
        var that = this;
        $.get("/main/tariffAndBalance", {"_": $.now()})
            .done(function (data) {
                that.tariffBlock.html(that.tariffBlockTemplateFn({
                    currentTariff: data.currentTariffPlan
                }));
                that.balanceInfo.html(that.balanceInfoBlockTemplateFn({
                    lastPayment: data.lastPayment,
                    currentBalance: data.balance
                }));
                if (data.currentTariffPlan.color == null || data.currentTariffPlan.color == "") {
                    $('#js-main-info').css('background-color', "#1b1d22");
                } else {
                    $('#js-main-info').css('background-color', data.currentTariffPlan.color);
                }
                that.refresh();
            })
            .error(function () {
                that.tariffBlock.html("<h2>Мой тариф</h2> " + JS_DATA["errorMainpageLoading"])
            })
    },
    loadDiscountsBlock: function () {
        var that = this;
        $.get('main/discounts', {"_": $.now()})
            .done(function (data) {
                that.emptyDiscounts = data.emptyDiscounts;
                var $spinner = $('#js-services-block-spinner');
                $spinner.before(that.discountsInfoBlockTemplateFn(data));
                $spinner.remove();
                that.placeServiceManageLink();
                if (data.twoOrMoreRows) {
                    $('#js-payment-section-short').removeClass('short-section');
                }
                that.refresh()
            });
    },
    closeSection: function (evt) {
        evt.$el.closest('.row').hide();
    },
    cancelPopup: function (evt) {
        $.arcticmodal('close');
    },
    showConfirmUpsaleForm: function (evt) {
        $('#js-request_report_confirm').arcticmodal({
            closeOnEsc: false,
            closeOnOverlayClick: false,
            afterOpen: function () {
                cufonReplace();
            },
            afterClose: function (data, el) {
                el.hide();
            }
        }).show();
    },
    makeUpsaleJump: function () {
        $.post("/makeUpsaleJump")
            .done(function (data) {
                $(document).trigger('showMessageBox', [{
                    title: "Успешно",
                    text: data.responseText,
                    autoClose: true
                }])
            })
            .fail(function (data) {
                if (data.status == '402') {
                    $('#js-upsale-payment-banner').show();
                } else {
                    $(document).trigger('showMessageBox', [{
                        title: "Ошибка",
                        type: data.responseText,
                        autoClose: true
                    }]);
                }
            });
        $.arcticmodal('close');
    }
});