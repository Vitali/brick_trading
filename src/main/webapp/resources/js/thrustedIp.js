var widgetTrustedIp = function () {

    var vm = {
        //urls come from trustedIps.ftl
        ajaxURLs: {
            add: "/",
            edit: "/",
            del: "/"
        },

        init: function (urls) {
            if (!_.isEmpty(urls)) {
                this.ajaxURLs = urls;
            }
            var self = this;
            var $table = $('table');
            this.saveIpButton = $('a.save-ip').hide();

            //adding new ip
            $table.on('click', 'a.add-ip', function () {
                var trustedIp = {ip: $('#new-ip').val()};
                self.ajaxRequest(trustedIp, self.ajaxURLs.add, self.addRow);
            });

            //deleting ip
            $table.on('click', 'a.delete-ip', function () {
                var trustedIp = {ip: $(this).closest('tr').data('val')};
                var rowToDelete = $(this).closest('tr');
                self.ajaxRequest(trustedIp, self.ajaxURLs.del, function () {
                    $(rowToDelete).hide(50);
                });
            });

            //editing ip
            $table.on('click', 'a.edit-ip', function () {
                $(this).closest('tr').find('td .form-control').removeAttr('readonly');
                $(this).hide();
                $(this).closest('tr').find('td a.save-ip').show();
            });

            //saving ip
            $table.on('click', 'a.save-ip', function () {
                var $rowForUpdate = $($(this).closest('tr'));
                var oldIp = $rowForUpdate.data('val');
                var newIp = $rowForUpdate.find('td .form-control').val();
                self.ajaxRequest({oldIp: oldIp, newIp: newIp}, self.ajaxURLs.edit,
                    function () {
                        $rowForUpdate.find('td .form-control').val(newIp).attr('readonly', true);
                        $rowForUpdate.data('val', newIp);
                        $rowForUpdate.find('a.save-ip').hide();
                        $rowForUpdate.find('a.edit-ip').show();
                    },
                    function () {
                        $rowForUpdate.find('td .form-control').val(oldIp).attr('readonly', true);
                        $rowForUpdate.find('a.save-ip').hide();
                        $rowForUpdate.find('a.edit-ip').show();
                    }
                );
            });
        },

        //creating row with added IP
        addRow: function (data) {
            var addedIpRow = $('#rowTemplate').html();
            var addedIpRowFn = doT.template(addedIpRow);
            var row = addedIpRowFn({ip: data.message});
            $(row).insertBefore('tr.new-ip-row');
            $('table tr.ip-row').last().find('a.save-ip').hide();
            $('#new-ip').val('');
        },

        ajaxRequest: function (sendData, url, successCallback, errorCallback) {
            var msgOptions = {};
            $.ajax({
                url: url,
                cache: false,
                type: "POST",
                crossDomain: true,
                dataType: 'json',
                data: sendData,
                success: function (data) {
                    successCallback(data);
                },
                error: function (data) {
                    var text = JSON.parse(data.responseText).message;
                    msgOptions = {
                        type: "error",
                        title: "Операция завершилась ошибкой",
                        autoClose: true,
                        text: function () {
                            if (text) {
                                return text;
                            } else {
                                return "";
                            }
                        }
                    };
                    $(document).trigger('showMessageBox', msgOptions);
                    errorCallback();
                }
            });
        }
    };

    return vm;
};
