/**
 * Created by blvp on 09.06.15.
 */
var extend = {
    Page: function (prototypeProps) {
        return function (staticOptions) {
            return {
                initialize: function (callback) {
                    _.extend(this, prototypeProps);
                    _.extend(this, staticOptions);
                    _.extend(this, _.object(_.map(this.components, function(selector, key){return [key, $(selector)];})));

                    if (!_.isUndefined(callback) && _.isFunction(callback)) {
                        callback();
                    }

                    this.refresh();
                },
                onRefresh: function(){
                },
                refresh: function () {

                    function registerFunction(that, eventhandler) {
                        return function (evt) {
                            evt.that = that;
                            evt.$el = $(evt.currentTarget);
                            that[eventhandler](evt);
                        }
                    }

                    var events = this.delegatedEvents;
                    var that = this;
                    if (events) {
                        _.each(events, function (eventHandler, eventUnparsed) {
                            var eventParsed = eventUnparsed.split(" ");
                            if (eventParsed.length < 2) {
                                console.warn("delegatedEvent: [ " + eventUnparsed + " ] should contain event pairs");
                            }
                            var eventType = eventParsed[0],
                                eventSelector = eventParsed[1];
                            $(eventSelector).on(eventType, registerFunction(that, eventHandler))
                        });
                    } else {
                        console.warn("no delegated Events for current page");
                    }
                    this.onRefresh();
                }

            }
        };
    }

};

